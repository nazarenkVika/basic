<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PublicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      "Gothetour/css/style68b3.css?ver=1",

    "Gothetour/css/new/bootstrap.min.css",
    // "Gothetour/css/new/font-awesome.min.css",
    // "Gothetour/css/new/animate.min.css",
    // "Gothetour/css/new/owl.carousel.css",
    // "Gothetour/css/new/owl.theme.css",
    // "Gothetour/css/new/owl.transitions.css",
    // "Gothetour/css/new/style.css",
    // "Gothetour/css/new/responsive.css",
    ];
    public $js = [
    "Gothetour/js/slick68b3.js?ver=1",
    "Gothetour/js/animation/ScrollMagic68b3.js?ver=1",
    "Gothetour/js/animation/TweenMax68b3.js?ver=1",
    "Gothetour/js/animation/animation.gsap68b3.js?ver=1",
    "Gothetour/plugins/easy-fancybox/js/jquery.mousewheel.min4830.js?ver=3.1.12",
		"Gothetour/js/animation/mousewheel_action.js",
    "Gothetour/js/animation-home.js",
    "Gothetour/js/script_main.js",

    "Gothetour/js/newJs/jquery-1.11.3.min.js",
    "Gothetour/js/newJs/bootstrap.min.js",
    "Gothetour/js/newJs/owl.carousel.min.js",
    "Gothetour/js/newJs/jquery.stickit.min.js",
    "Gothetour/js/newJs/menu.js",
    "Gothetour/js/newJs/scripts.js",
    ];
    public $depends = [
        
    ];
}
