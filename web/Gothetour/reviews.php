<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru-RU" prefix="og: http://ogp.me/ns#" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<head>
		<title>
			Отзывы - INTHETOUR
		</title>
		<link rel="shortcut icon" href="../wp-content/themes/images/favicon.png" type="image/x-icon"/>
		<link href="../wp-content/uploads/bootstrap/bootstrap.min.css" rel="stylesheet">
		<!-- Прячет блоки  в слайдер, а не в список -->
		<link rel='stylesheet' id='style.owl.carousel-css'  href='../wp-content/plugins/owl-carousel/css/owl.carousele049.css?ver=4.7.8' type='text/css' media='all' />
		<!-- Вид next -->
		<link rel='stylesheet' id='style.owl.carousel.theme-css'  href='../wp-content/plugins/owl-carousel/css/owl.themee049.css?ver=4.7.8' type='text/css' media='all' />
		<link rel='stylesheet' id='style.owl.carousel.styles-css'  href='../wp-content/plugins/owl-carousel/css/stylese049.css?ver=4.7.8' type='text/css' media='all' />
		<!-- Отображение -->
		<script type='text/javascript' src='../wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
		<!-- Pred/next --><!-- Отображение все -->
		<script type='text/javascript' src='../wp-content/plugins/owl-carousel/js/owl.carousele049.js?ver=4.7.8'></script>
		<!-- Отображение все -->
		<script type='text/javascript' src='../wp-content/plugins/owl-carousel/js/scripte049.js?ver=4.7.8'></script>
	</head>
	<body class="page-template page-template-template-reviews page-template-template-reviews-php page page-id-2320">
		<link rel="stylesheet" href="../wp-content/themes/style68b3.css?ver=1" type="text/css">
		<link rel="stylesheet" href="../wp-content/themes/css/color-shemes/yellow-orange/yellow-orange.css" type="text/css">
		<header>
			<div id="header">
				<div class="wrap">
					<div class="button-group">
						<div class="call-phone" style="margin-left: 45px; margin-top: 50px;">
							<a href="tel:+375259033153;"></a>
						</div>
					</div>
					<div class="cloud">
						<div class="menu">
							<a class="navicon mtoggle" href="#">
							Меню
							</a>	
						</div>
					</div>
					<div id="logo-container" class="logo">
						<a id="logo" href="../index.html">
						<img src="../wp-content/themes/images/backgrounds/Logo.png" >
						</a>
					</div>
					<div class="menu">
						<div id="main-navigation" class="menu-mani-menu-container" style="margin-left: 280px;">
							<ul id="menu-mani-menu" class="dd-menu">
								<li id="menu-item-2619" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2619"><a href="../bron/index.html">Бронь</a></li>
								<li id="menu-item-2620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2620">
									<a href="#">О нас</a>
									<ul class="sub-menu">
										<li id="menu-item-3597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3597"><a title="INTHETOUR" href="../about/index.html">О компании INTHETOUR</a></li>
										<li id="menu-item-2622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2622"><a href="../blog/index.html">Блог</a></li>
									</ul>
								</li>
								<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065"><a href="../reviews/index.html">Отзывы</a></li>
								<li id="menu-item-2691" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2691"><a href="../contacts/index.html">Контакты</a></li>
								<li id="menu-item-3907" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3907"><a href="../hotels/index.html">Отели</a></li>
							</ul>
						</div>
					</div>
				</div>
				<style type="text/css">
					.qtranxs_widget { display: none; }
					.qtranxs_widget ul { z-index:99999999; margin: 0; }
					.qtranxs_widget ul li
					{
					display: inline; 
					list-style-type: none; 
					margin: 0 5px 0 0; 
					opacity: 0.5;
					-o-transition: 1s ease opacity;
					-moz-transition: 1s ease opacity;
					-webkit-transition: 1s ease opacity;
					transition: 1s ease opacity;
					}
					.qtranxs_widget ul li span { margin: 0 5px 0 0; } 
					.qtranxs_widget ul li.active { opacity: 0.8; }
					.qtranxs_widget ul li:hover { opacity: 1; }
					.qtranxs_widget img { box-shadow: none; vertical-align: middle; display: initial; }
					.qtranxs_flag { height:12px; width:18px; display:block; }
					.qtranxs_flag_and_text { padding-left:20px; }
					.qtranxs_flag span { display:none; }
				</style>
				<div id="qtranslate-2" class="widget qtranxs_widget">
					<ul class="language-chooser language-chooser-custom qtranxs_language_chooser" id="qtranslate-2-chooser">
						<li class="language-chooser-item language-chooser-item-ru active"><a href="index.html" title="Русский (ru)">ru</a></li>
					</ul>
					<div class="qtranxs_widget_end"></div>
				</div>
			</div>
		</header>
		<div id="content" class="reviews">
		<div class="wrap">
		<div class="с-12">
		<div class="page page-content">
		<div class="reviews-content">
		<div id="blog-background">
			<div id="content">
				<h1 class="block-title" style="margin-top: -8px;">Отзывы клиентов</h1>
			</div>
		</div>
		<div
			class="carousel-reviews">
		<div id="owl-carousel-677717799" class="owl-carousel owl-carousel-reviews-carousel"  data-singleitem="true"  data-autoplay="false"  data-navigation="true"  data-pagination="false" >
		<div class="item">
			<div class="owl-carousel-item-text">
				<h2>Отдых в Египте. Отель Xperience Kirosieiz Premier</h2>
				<h3>13.05.2019</h3>
				<blockquote cite="https://vk.com/topic-133517780_34387013">Отдых понравился всем. было очень интересно и в тоже время весело. Честно говоря отель не дотягивает до 5* но это не вина операторов. Хочу поблагодарить Викторию Назаренко, она в очень малый промежуток времени предоставила очень много вариантов где можно отдохнуть. Так-же Виктория предоставила несколько десяток отелей что позволило нам выбирать и смотреть где лучше и какие есть развлечение. Еще она дала напутственную инструкцию по отдыха)) с кем можно спорить, где можно гулять и где лучше всего нужно побывать в первую очередь.Подвожу итоги - отдых понравился, СПАСИБО Виктория Назаренко что ты есть и даешь хорошие советы. Советую обращаться к ней!!!</blockquote>
			</div>
		</div>
		<div class="item">
			<div class="owl-carousel-item-text">
				<h2>Спасибо вам за все! STELLA DI MARE BEACH RESORT & SPA MAKADI 5*</h2>
				<h328.02.2019</h3>
				<blockquote cite="https://vk.com/topic-133517780_34387013">В общем нам все очень понравилось. Отель больше для тихого и спокойного отдыха, но если поехать компанией скучно не будет точно! Порадовало конечно питание: меню разнообразное. Твёрдая 5 за отель, можете смело рекомендовать тем кто ни раз был уже в Египте, думаю что понравиться.Спасибо вам за все!</blockquote>
			</div>
		</div>
		<div class="item">
			<div class="owl-carousel-item-text">
				<h2>Первый и удачный опыт путешествий</h2>
				<h3>15.02.2019</h3>
				<blockquote cite="https://vk.com/topic-133517780_34387013">Итак, начну с того, что меня обслуживала Виктории Назаренко. Человек, знающий свое дело! Мы очень переживали с подругой, что что-то не так сделаем, ведь впервые летели на отдых, но Виктория на протяжении всего времени была с нами, все рассказывала, особенно успокаивало то, что Виктория сама была в Египте и поэтому мы все делали сразу, как она и сказала. Отель подобрала замечательный (Алоха), места в самолёте были самые комфортные, мы смогли вытянуть ножки, в общем, я скажу так: обращайтесь к Виктории, она все вам доступно объяснит, даже таким паникующим людям, как я. А отдых от INTHETOUR мы никогда не забудем, все было на высшем уровне, спасибо.
				</blockquote>
			</div>
		</div>
		<div class="item">
			<div class="owl-carousel-item-text">
				<h2>Отдых в отеле Caves Beach Resort Хургада</h2>
				<h3>23.11.2018</h3>
				<blockquote cite="https://vk.com/topic-133517780_34387013">
				Огромное спасибо за подбор отеля! Это действительно оригинально и то, что нам идеально подошло! Сервис прекрасный, атмосфера замечательная! Ветра не беспокоили(что удивительно для сезона). Рейсы по расписанию. Зря я волновалась насчёт авиакомпаний и отзывов)) Даже фотосессию на память сделали в стиле отеля)персонал очень дружелюбный, а в ресторанах вкусно и разнообразно.
			</div>
		</div>
		<div class="item">
			<div class="owl-carousel-item-text">
				<h2>Rehana royal beach resort</h2>
				<h3>27.07.2018</h3>
				<blockquote cite="https://vk.com/topic-133517780_34387013">Хочу поблагодарить компанию INTHETOUR,а в частности менеджера Наиру,за великолепно проведённое время на берегу Красного моря.Доверились мнению Наиры в выборе отеля «Rehana royal beach resort « и не прогадали.Доступная цена,красивый и уютный отель.Спасибо огромное)))</blockquote>
			</div>
		</div>
	</body>
</html>