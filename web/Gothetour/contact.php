<!DOCTYPE html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>
			Турагентство INTHETOUR
		</title>
		<link rel="shortcut icon" href="../wp-content/themes/images/favicon.png" type="image/x-icon"/>
		<!--отображение -->
		<script type='text/javascript' src='../wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
		<script type='text/javascript' src='../wp-content/themes/slick/slick68b3.js?ver=1'></script>
		<!--скрол колесом -->
		<script type='text/javascript' src='../wp-content/themes/js/animation/ScrollMagic68b3.js?ver=1'></script>
		<script type='text/javascript' src='../wp-content/themes/js/animation/animation.gsap68b3.js?ver=1'></script>
		<!-- Отвечают за скрол ручками -->
		<script src="../wp-content/themes/js/animation/mousewheel_action.js"></script>
		<script type='text/javascript' src='../wp-content/plugins/easy-fancybox/js/jquery.mousewheel.min4830.js?ver=3.1.12'></script>
		<!--Про кнопке поехали автоматически-->
		<script type='text/javascript' src='../wp-content/themes/js/animation/TweenMax68b3.js?ver=1'></script>
	</head>
	<body class="home blog">
		<link rel="stylesheet" href="../wp-content/themes/style68b3.css?ver=1" type="text/css">
		<header>
			<div id="header">
				<div class="wrap">
					<div class="button-group">
						<div class="call-phone" style="margin-left: 45px; margin-top: 50px;">
							<a href="tel:+375259033153;"></a>
						</div>
					</div>
					<div class="cloud">
						<div class="menu">
							<a class="navicon mtoggle" href="#">
							Меню
							</a>	
						</div>
					</div>
					<div id="logo-container" class="logo">
						<a id="logo" href="../index.html">
						<img src="../wp-content/themes/images/backgrounds/Logo.png">
						</a>
					</div>
					<div class="menu">
						<div id="main-navigation" class="menu-mani-menu-container" style="margin-left: 280px;">
							<ul id="menu-mani-menu" class="dd-menu">
								<li id="menu-item-2619" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2619"><a href="../bron/index.html">Бронь</a></li>
								<li id="menu-item-2620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2620">
									<a href="#">О нас</a>
									<ul class="sub-menu">
										<li id="menu-item-3597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3597"><a title="INTHETOUR" href="../about/index.html">О компании INTHETOUR</a></li>
										<li id="menu-item-2622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2622"><a href="../blog/index.html">Блог</a></li>
									</ul>
								</li>
								<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065"><a href="../reviews/index.html">Отзывы</a>
								</li>
								<li id="menu-item-2691" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2691"><a href="../contacts/index.html">Контакты</a></li>
								<li id="menu-item-3907" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3907"><a href="../hotels/index.html">Отели</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- карта -->
		<div id="contacts-page" style="margin-top: -100px;">
			<div id="blog-background">
				<div id="content">
					<h1 class="block-title" >Контакты</h1>
				</div>
			</div>
			<div class="map-wrapper">
				<div id="map-canvas"  style="margin-top: -20px;">
					<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A8149ad126a8ccb16525fa6201b3765ffbb351183ee54ff425c607697babd4106&amp;source=constructor" width="100%" height="470" frameborder="0"></iframe>
				</div>
			</div>
		</div>
	</body>
</html>