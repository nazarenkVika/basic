jQuery(document).ready(function () {
    if (jQuery(window).width() < 768) {
        jQuery("#menu-mani-menu").hide(), jQuery(".mtoggle").click(function () {
            jQuery("#menu-mani-menu").slideToggle(200)
        }), jQuery(".sign-block a").each(function () {
            jQuery(this).siblings("p").remove(), jQuery("#removed_menu").remove(), jQuery(this).wrap("<li></li>"), jQuery(this).parent().appendTo("#menu-menu-1")
        });
        var e = "ontouchstart" in window ? "click" : "mouseenter mouseleave";
        jQuery(".dd-menu > li:first-child").addClass("closed"), jQuery(".dd-menu > li:first-child").on(e, function () {
            jQuery(this).toggleClass("closed")
        }), jQuery(".dd-menu > li:nth-child(5)").addClass("closed"), jQuery(".dd-menu > li:nth-child(5)").children(".sub-menu").hide(), jQuery(".dd-menu > li:nth-child(5)").on(e, function () {
            jQuery(this).toggleClass("closed"), jQuery(this).children(".sub-menu").toggle()
        })
    }
}),
function (e) {
    var u = e(".dd-menu li:first-child > .sub-menu");
    u.show();
    var n = e(".dd-menu li:first-child > .sub-menu .menu-item a").outerHeight();
    u.removeAttr("style"), e(window).on("load", function () {
        e.mCustomScrollbar.defaults.scrollButtons.enable = !0, u.mCustomScrollbar({
            theme: "dark3"
        }), e(".mCustomScrollbar #mCSB_1").height(10 * n + 10), e(".contacts-menu .sub-menu").mCustomScrollbar({
            theme: "dark3"
        }), e(".mCustomScrollbar #mCSB_2").height(10 * n + 10)
    }), e(window).on("load", function () {
        e(window).height() < 480 && (e.mCustomScrollbar.defaults.scrollButtons.enable = !0, e("#menu-menu-1").mCustomScrollbar({
            theme: "dark3"
        }), e("#menu-menu-1.mCustomScrollbar").height(270))
    })
}(jQuery), jQuery(".menu .sub-menu").on({
    mousewheel: function (e) {
        "el" != e.target.id && (e.preventDefault(), e.stopPropagation())
    }
});