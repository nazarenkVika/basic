$(document).ready(function() {
    $('.minus').click(function () {
        var $input = $("#counter");
        var count = parseInt($input.val()) + 1;
        count = count < $input.attr("max") ? $input.attr("max") : count;
        console.log(count);
        console.log($input.attr("max"));
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $("#counter");
        var count = parseInt($input.val()) + 1;
        count = count > $input.attr("max") ? $input.attr("max") : count;
        console.log(count);
        console.log($input.attr("max"));
        $input.val(count);
        $input.change();
        return false;
    });
});