;(function($) {
    "use strict";
    var windowHeight;
    var windowWidth;

    var video_url_mobile = $("#videoMobileFrame").attr("src");

    $(".modalBackground, #modalVideo .close, #videoMobile .close").on("click", function() {
        $("#modalVideo").slideUp("200");
        $("#videoMobile").slideUp("200");
        $(".modalBackground").fadeOut("200");
    });
    $("#overlay").click(function() {
        $("#videoMobile").slideDown("200");
        $(".modalBackground").fadeIn("200");
        $("#videoMobileFrame").attr("src", video_url_mobile);
        $("#videoMobileFrame")[ 0 ].src += "&autoplay=1";
    });
    $("#videoMobile .close, .modalBackground, #modalVideo .close").on("click", function() {
        $("#videoMobileFrame, #modalVideo iframe").attr("src", "");
    });
    $("#slick-slide03 button").click(function() {
        $("#video-mobile").fadeIn();
    });
    $("#slick-slide00 button, #slick-slide01 button, #slick-slide02 button, #slick-slide04 button").click(function() {
        $("#video-mobile").fadeOut();
    });

    $("#slick-slide04 button").click(function() {
        $("#socials, #board-links").show();
    });
    $("#slick-slide00 button, #slick-slide01 button, #slick-slide02 button, #slick-slide03 button").click(function() {
        $("#socials, #board-links").hide();
    });

    function checkLandscape() {
        console.log("checkLandscape func");
        windowWidth = window.innerWidth;

        $("html, body").animate({ scrollLeft: 0 }, "0");
        if ( windowWidth > 767 ) {
            if ( window.innerHeight > window.innerWidth ) {
                $("#main-wrapper").css({ "width": "100%" });
                $("#modalWindowWarning, .modalBackground").show();
            } else if ( window.innerHeight <= window.innerWidth ) {
                $("#modalWindowWarning, .modalBackground").hide();
            }
        } else if ( windowWidth <= 767 ) {
            TweenMax.killAll();
            if ( window.innerHeight > window.innerWidth ) {
                $("#modalWindowWarningLand, .modalBackground").hide();
            } else if ( window.innerHeight <= window.innerWidth ) {
                $("#modalWindowWarningLand, .modalBackground").show();
            }
        }
        responsiveValue();
    }

    // Responsitivity
    // console.log("main js is working");
    var horizonImg = $(".horizon img");

    // Common functions (start)

    // windowHeight = $(window).outerHeight();
    function animateOnWidth() {
        windowWidth = jQuery(window).width();
        windowHeight = jQuery(window).outerHeight();
        console.log("animate on width started");
        var end = horizonImg.get(0).scrollWidth;
        var width_to_height;

        if ( windowWidth / windowHeight >= 2.2 ) {
            width_to_height = ( ( windowHeight / 100 ) / ( windowWidth / 100 ) ) * 1400;
        } else if ( windowWidth / windowHeight >= 2 && windowWidth / windowHeight < 2.2 ) {
            width_to_height = ( ( windowHeight / 100 ) / ( windowWidth / 100 ) ) * 1050;
        } else if ( windowWidth / windowHeight >= 1.9 && windowWidth / windowHeight < 2 ) {
            width_to_height = ( ( windowHeight / 100 ) / ( windowWidth / 100 ) ) * 800;
        } else if ( windowWidth / windowHeight >= 1.8 && windowWidth / windowHeight < 1.89999 ) {
            width_to_height = ( ( windowHeight / 100 ) / ( windowWidth / 100 ) ) * 750;
        } else if ( windowWidth / windowHeight >= 1.7 && windowWidth / windowHeight < 1.8 ) {
            width_to_height = ( ( windowHeight / 100 ) / ( windowWidth / 100 ) ) * 450;
        } else if ( windowWidth / windowHeight >= 1.65 && windowWidth / windowHeight < 1.7 ) {
            width_to_height = ( ( windowHeight / 100 ) / ( windowWidth / 100 ) ) * 580;
        } else {
            width_to_height = ( ( windowHeight / 100 ) / ( windowWidth / 100 ) ) * 120;
        }

        // console.log("Proportions: " + windowWidth / windowHeight);
        // console.log("windowWidth: " + windowWidth);
        var page = $("html, body");

        $("#boy").animate({
            left: 0,
            marginLeft: width_to_height
        }, 4000, function() {
            speedUpToRight();
        });

        console.log("animate on width 1");
        page.animate({
            scrollLeft: end
        }, 7000);
        console.log("animate on width 2");
        // page.css({
        //     "overflow": "hidden"
        // }, 7000);
        TweenMax.killAll(); // TODO Uncomment to play animation once
        // console.log("animation killed");
        // page.css({
        //     "overflow": "visible"
        // });

        setTimeout(function() {
            $(document).scroll(function() {
                if ( $(document).scrollLeft() > 5 ) {
                    $("#boy img").attr("src", "../../images/boy/people1.svg");
                    $("#boy img").css('height', windowHeight/2.10930061);
                    $("#segway-stoping").hide();
                }
            });
        }, 7000);
        rocketThings();
        // console.log("rocket animation returned");
    }

    $(window).scroll(function() {
        $("#video").css({
            "pointer-events": "none"
        });
        clearTimeout($.data(this, "scrollCheck"));
        $.data(this, "scrollCheck", setTimeout(function() {
            $("#video").css({
                "pointer-events": "initial"
            });
        }, 300));
    });

    rocketThings();

    function percentToPixel(_elem, _perc) {
        console.log("percent to pixel started");
        return ($(_elem).parent().outerWidth() / 100) * parseFloat(_perc);
    }

    function speedUpToRight() {
        console.log("animate on width 3");
        var boy_img = $("#boy img");
        boy_img.attr("src", "/wp-content/themes/kids_new/images/boy/boy9.svg");
        $("#segway-stoping").show();

        var boyBottom = windowHeight / 16.8571428;
        var boyHeight = windowHeight / 2.39385475;
        var boyRight = windowHeight / 1.09871795;

        var boyBottomResize = windowHeight / 9.52222222;
        var boyHeightResize = windowHeight / 1.90444444;

        boy_img.attr("src", "/wp-content/themes/kids_new/images/boy/boy-jump.svg").css({
            "height": boyHeightResize,
            "bottom": boyBottomResize
        });

        var boy_item = $("#boy-item");

        boy_item.show();

        var old_top = parseInt(boy_item.css("top"));
        var new_top = parseInt(boy_item.css("top")) - 160;

        boy_item.animate({
            top: new_top + "px"
        }, 200);
        boy_item.animate({
            top: old_top + "px"
        }, 200, function() {
            console.log("animate on width 4");
            boy_item.hide();
            boy_img.attr("src", "../../images/boy/people1.svg").css({
                "height": boyHeight,
                "bottom": boyBottom,
                "right": boyRight
            });
            console.log("animate on width 5");
        });
    }

    // Common functions (end)
    function responsiveValue() {
        console.log("responsive value started");

        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        var header = $("header #header");
        windowHeight = $(window).height();
        windowWidth = $(window).width();
        if ( iOS == true && window.innerHeight <= window.innerWidth ) {
            header.css({
                left: "120px"
            });
        } else {
            header.css({
                left: "0"
            });
        }
        header.css("width", windowWidth);

        // Responsive Background
        var horizonImgHeight = horizonImg.height();
        var horizonImgWidth = horizonImg.width();

        $(".front img").css({
            "width": horizonImgWidth
        });
        $("#main-wrapper").css("width", horizonImgWidth);

        var horizonHeight = $(".horizon").outerHeight();
        // console.log("Your Screen Height: " + windowHeight);
        // console.log("Your Screen Width: " + windowWidth);

        // Responsive Elements
        // Billboard 1
        var billboard1Left = windowHeight / 0.95696489;
        var billboard1Top = windowHeight / 4.0212766;
        var billboard1Width = windowHeight / 1.49822695;
        var billboard1Height = windowHeight / 2.9137931;

        $("#billboard1").css({
            "left": billboard1Left,
            "top": billboard1Top,
            "width": billboard1Width,
            "height": billboard1Height
        });

        // Billboard 2
        var billboard2Left = windowHeight / 0.40066382;
        var billboard2Top = windowHeight / 3.89690722;
        var billboard2Width = windowHeight / 1.49822695;
        var billboard2Height = windowHeight / 2.9137931;

        $("#billboard2").css({
            "left": billboard2Left,
            "top": billboard2Top,
            "width": billboard2Width,
            "height": billboard2Height
        });

        // Woman
        var womanLeft = windowHeight / 1.48135593;
        var womanBottom = horizonHeight / 5.51445087;
        var womanHeight = windowHeight / 4.64848485;

        $("#woman").css({
            "margin-left": womanLeft,
            "bottom": womanBottom,
            "height": womanHeight
        });

        // Car
        var carLeft = windowHeight / 0.20704225;
        var carBottom = horizonHeight / 5.51445087;
        var carHeight = windowHeight / 9.42307692;

        $("#car").css({
            "margin-left": carLeft,
            "bottom": carBottom,
            "height": carHeight
        });

        // Bus
        var busLeft = windowHeight / 0.13521368;
        var busBottom = horizonHeight / 5.51445087;
        var busHeight = windowHeight / 4.65294118;

        $("#bus").css({
            "margin-left": busLeft,
            "bottom": busBottom,
            "height": busHeight
        });

        // Truck
        var truckLeft = windowHeight / 3.146;
        var truckBottom = horizonHeight / 5.51445087;
        var truckHeight = windowHeight / 5.45517241;

        $("#truck").css({
            "margin-left": -truckLeft,
            "bottom": truckBottom,
            "height": truckHeight
        });

   // Aero
   var AeroLeft = windowHeight / 1;
   var AeroBottom = horizonHeight / 1;
   var AeroHeight = windowHeight / 1;

   $("#Aero").css({
       "margin-left": -AeroLeft,
       "bottom": AeroBottom,
       "height": AeroHeight
   });

       

        // Smoke
        var smokeWidth = windowHeight / 21.425;
        var smokeHeight = windowHeight / 1.3496063;
        var smokeBottom = windowHeight / 9.02105263;
        var smokeLeft = windowHeight / 0.18292423;

        $("#smoke").css({
            "width": smokeWidth,
            "height": smokeHeight,
            "bottom": smokeBottom,
            "left": smokeLeft
        });

        // Socials

        var socialsLeft = windowHeight / 0.18063044;
        var socialsTop = windowHeight / 2.1775;
        var socialsWidth = windowHeight / 3.78695652;

        $("#desktop-socials").css({
            "left": socialsLeft,
            "top": socialsTop,
            "width": socialsWidth
        });

        // Rocket
        var rocketWidth = windowHeight / 2.72839506;
        var rocketHeight = windowHeight / 1.2509434;
        var rocketBottom = horizonHeight / 8.2875;
        var rocketRight = horizonHeight / 2.7625;

        $("#rocket").css({
            "width": rocketWidth,
            "height": rocketHeight,
            "bottom": rocketBottom,
            "right": rocketRight
        });

        // Boy Trigger
        var boyTriggerLeft = windowHeight / 4.125;

        $("#triggerBoy").css({
            "left": boyTriggerLeft
        });

        // Boy
        var boyLeft = windowHeight / 4.96842105;
        var boyBottom = windowHeight / 16.8571428;
        var boyWidth = windowHeight / 7.26153846;
        var boyHeight = windowHeight / 2.60869565;

        if ( iOS == false ) {
            // console.log("You are not using iOS");
            $("#boy").css({
                "left": boyLeft,
                "bottom": "45%",//boyBottom,
                "width": boyWidth
            });
        } else {
            $("#boy").css({
                "left": "400px",
                "bottom": "45%",//boyBottom,
                "width": boyWidth
            });
        }
        $("#boy img").css({
            "height": boyHeight
        });

        // Segway Stoped First
        var segwayStopedLeft = percentToPixel(horizonImg, 15);
        var segwayStopedBottom = windowHeight / 12.6923076;
        var segwayStopedWidth = windowHeight / 10.3125;

        $("#segway-stoped").css({
            "left": segwayStopedLeft,
            "bottom": segwayStopedBottom,
            "width": segwayStopedWidth
        });

        // Segway Boy One
        var segwayBoy1Left = windowHeight / 0.78970719;
        var segwayBoy1Bottom = windowHeight / 12.6923076;
        var segwayBoy1Height = windowHeight / 2.28205128;

        $("#boy-segway1").css({
            "left": segwayBoy1Left,
            "bottom": segwayBoy1Bottom,
            "height": segwayBoy1Height
        });

        // Segway Trigger
        var segwayTriggerLeft = percentToPixel(horizonImg, 15);

        $("#triggerSegway").css({
            "left": segwayTriggerLeft
        });

        // Segway Stoping
        var segwayStopingRight = windowHeight / 1.26197183;
        var segwayStopingBottom = windowHeight / 13.0952381;
        var segwayStopingHeight = windowHeight / 3.92857143;

        $("#segway-stoping").css({
            "right": segwayStopingRight,
            "bottom": segwayStopingBottom,
            "height": segwayStopingHeight
        });

        // Boy Jump
        var boyJumpRight = windowHeight / 1.18705036;
        var boyJumpBottom = windowHeight / 12.2739726;
        var boyJumpHeight = windowHeight / 2.60869565;

        $("#boy-jump").css({
            "right": boyJumpRight,
            "bottom": boyJumpBottom,
            "height": boyJumpHeight
        });

        // Trigger Jump
        var triggerJumpRight = percentToPixel($("#triggerJump"), 30);

        $("#triggerJump").css({
            "right": triggerJumpRight
        });

        // Boy Item
        var boyItemRight = windowHeight / 0.806639;
        var boyItemBottom = windowHeight / 2.62738854;
        var boyItemHeight = windowHeight / 12.1323529;

        $("#boy-item").css({
            "right": boyItemRight,
            "bottom": boyItemBottom,
            "height": boyItemHeight
        });

        // Trigger Item
        var triggerItemRight = windowHeight / 0.36536797;

        $("#triggerItem").css({
            "right": triggerItemRight
        });

        // Column
        var columnRight = windowHeight / 24.1666666;
        var columnBottom = windowHeight / 5.53435115;
        var columnHeight = windowHeight / 1.8125;
        var columnWidth = windowHeight / 3.45238095;

        $("#column").css({
            "right": columnRight,
            "bottom": columnBottom,
            "height": columnHeight,
            "width": columnWidth
        });
//добавляем классы в анимацию
        // Add animation class
        setTimeout(function() {
            var array = [
                ".cloud",
                "#woman",
                "#car",
                "#bus",
                "#truck",
                "#Aero"
            ];
            array = array.join(",");
            $(array).addClass("animated");
        }, 2000);

        // Rocket is flying away when click one of the boards
        // rocketThings();
    }

    function rocketThings() {
        if ( windowWidth >= 768 ) {
            console.log("rocket things started");

            var allBoards = $("#first-board, #second-board, #third-board, #fourth-board");
            var allBoardsTexts = $("#register-child, #closest-school, #check-schedule, #trial-month");
            allBoards.css("pointer-events", "initial");
            allBoardsTexts.css("pointer-events", "initial");
            allBoards.add(allBoardsTexts).on("click", function(e) {

                // Preventing other boards from click
                allBoards.css("pointer-events", "none");
                allBoardsTexts.css("pointer-events", "none");

                // Add animation to rocket
                setTimeout(function() {
                    $("#rocket").addClass("animated");
                }, 1500);
                var link = $(this).attr("xmlns:xlink");

                // Blocking href
                e.preventDefault();

                // Redirect after clicking in 3 sec
                setTimeout(function() {
                    window.location.href = link;
                }, 3000);
                if ( windowWidth > 767 ) {
                    var boyGoesAway = new TimelineMax({
                        paused: true
                    });
                    var boyAwayArray = [
                        "/wp-content/themes/kids_new/images/boy/boy5.svg",
                        "/wp-content/themes/kids_new/images/boy/boy6.svg",
                        "/wp-content/themes/kids_new/images/boy/boy5.svg",
                        "Gothetour/images/boy/people1.svg"
                    ];
                    var stay = { curStay: 0 };
                    // create tween
                    boyGoesAway.to(stay, 1,
                        {
                            curStay: boyAwayArray.length - 1,
                            roundProps: "curStay",
                            repeat: 0,
                            immediateRender: true,
                            ease: SteppedEase.config(4),
                            css: {
                                transform: "translateX(" + windowHeight / 2.3691395 + "px)"
                            },
                            onUpdate: function() {
                                $("#boyImg").attr("src", boyAwayArray[ stay.curStay ]);
                            }
                        }
                    );
                    var boyGoesRightLine = new TimelineMax();
                    boyGoesRightLine.to("#boyImg", 1,
                        {
                            css: {
                                transform: "translate(" + windowHeight / 2.3691395 + "px, -12vh)",
                                display: "none"
                            }
                        }
                    );
                    $("body").on("mousewheel", function(event, delta) {
                        return false;
                    });
                    boyGoesRightLine.play();
                    boyGoesAway.play();
                }
            });
        }
    }

    function mainJS() {
        console.log('main js func');
        windowHeight = $(window).height();
        windowWidth = $(window).width();

        // Animation on scroll
        function mainAnimationOnScroll() {
            console.log("mainAnimationOnScroll started");
            windowHeight = $(window).height();
            windowWidth = $(window).width();

            // init controller
            var controller = new ScrollMagic.Controller({
                vertical: false
            });



            //приостановка горизонтального скролла и запуск вертикального скролла
            function stopHorisontalScroll(){
                controller.scrollTo(controller.info('scrollPos'));
            }
            function startVerticalScroll(event){
                var delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
                if (delta >= 0) {
                    $(this).scrollTop($(this).scrollTop() - 50);
                } else {
                    $(this).scrollTop($(this).scrollTop() + 50);
                }
            }
            $('.sub-menu').bind('mouseover', function(){
                $(window).bind('scroll', stopHorisontalScroll);
                $('.sub-menu').bind('mousewheel', startVerticalScroll);
            }).bind('mouseout', function(){
                $(window).unbind('scroll', stopHorisontalScroll);
                $('.sub-menu').unbind('mousewheel', startVerticalScroll);
            });
            //конец приостановка горизонтального скролла и запуск вертикального скролла





            // Boy change
            var boyImgArray = [
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                /*перед зданием*/
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",,
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
                "../Gothetour/images/boy/people1.svg",
            ];
            var obj = { curImg: 0 };
            // create tween
            var boy = TweenMax.to(obj, 1.5,
                {
                    curImg: boyImgArray.length - 1,
                    roundProps: "curImg",
                    ease: SteppedEase.config(64),
                    onUpdate: function() {

                        // For unpredictable errors in showing images
                        if ( boyImgArray[ obj.curImg ] == "../Gothetour/images/boy/people1.svg"
                            || boyImgArray[ obj.curImg ] == "../Gothetour/images/boy/people1.svg" ) {

                            $("#boyImg").css({
                                "height": windowHeight / 2.1425,
                                "bottom": windowHeight / 15.5818181
                            });

                            $("#segway-stoped, #segway-stoping").hide();
                        } else {
                            if ( $("#segway-stoped:visible, #segway-stoping:visible").length == 0 ) {

                                $("#boyImg").css({
                                    "height": windowHeight / 2.60869565,
                                    "bottom": windowHeight / 16.8571428
                                });

                                $("#segway-stoped, #segway-stoping").show();
                            }
                        }
                        // End

                        $("#boyImg").attr("src", boyImgArray[ obj.curImg ]);
                    }
                }
            );
            var segwayHide = new TimelineMax().to("#segway-stoped", 1,
                {
                    css: {
                        display: "none"
                    },
                    ease: Linear.easeNone
                }
            );
            // var boyHide = new TimelineMax().to("#boyImg", 1,
            //     {
            //         css: {
            //             //                display: "none"
            //         },
            //         ease: Linear.easeNone
            //     }
            // );
            var segway = { curSeg: 0 };
            var segwayMoveValue = windowHeight / 0.20200784; // 4084px
            var segwayMove = new TimelineMax()
                .to("#boy-segway1", 1,
                    {
                        css: {
                            transform: "translateX(" + segwayMoveValue + "px)",
                            display: "none"
                        },
                        ease: Linear.easeNone
                    }
                )
                .to("#boy-segway1", 0.00001,
                    {
                        css: {
                            display: "none"
                        },
                        ease: Linear.easeNone
                    }
                );
            var itemMove = new TimelineMax()
                .to("#boy-item", 1,
                    {
                        css: {
                            display: "block"
                        },
                        ease: Linear.easeNone,
                        onComplete: animateOnWidth
                    }
                );

            // build scene
            if ( windowWidth > 480 ) {
                var triggerHookValue = windowWidth / 9520;
                var boySceneDuration2 = windowHeight / 2.5;
                var scene = new ScrollMagic.Scene({ triggerElement: "#triggerBoy", duration: boySceneDuration2 + "%" }) // 33%
                    .setTween(boy)
                    .addTo(controller);
                scene.triggerHook(triggerHookValue);

                // var boyHidden = new ScrollMagic.Scene({ triggerElement: "#triggerSegway", duration: 0.00001 })
                //     .setTween(boyHide)
                //     .addTo(controller);
                // boyHidden.triggerHook(triggerHookValue);

                var segwayHidden = new ScrollMagic.Scene({ triggerElement: "#triggerSegway", duration: 0.00001 })
                    .setTween(segwayHide)
                    .addTo(controller);
                segwayHidden.triggerHook(triggerHookValue);

                var MoveSegwayDuration = windowHeight / 4.52550741;
                var MoveSegway = new ScrollMagic.Scene({
                    triggerElement: "#triggerSegway",
                    duration: MoveSegwayDuration + "%"
                }) // 182.3% == 3500(?)
                    .setTween(segwayMove)
                    .addTo(controller);
                MoveSegway.triggerHook(triggerHookValue);

                var itemMoveUpDownDuration = windowHeight / 351.063829;
                var itemMoveUpDown = new ScrollMagic.Scene({
                    triggerElement: "#triggerItem",
                    duration: itemMoveUpDownDuration + "%"
                }) // 2.35%
                    .setTween(itemMove)
                    .addTo(controller);
                itemMoveUpDown.triggerHook(triggerHookValue);

            }
        } // .end mainAnimationOnScroll()

        $(window).load(function() { // TODO Fix before Prod (bug on mozila document load) (P.S. looks like fixed...)
            console.log("window load_______________");
            // checkLandscape();
            $("html, body").one().animate({ scrollLeft: 0 }).promise().then(function() {
                mainAnimationOnScroll();
            });

            // refresh page when push "back" button on safari
            window.addEventListener("pageshow", function(evt) {
                if ( evt.persisted ) {
                    setTimeout(function() {
                        window.location.reload();
                    }, 10);
                }
            }, false);
            checkLandscape();
        });

        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

        if ( iOS === true ) {
            $("body").addClass("ios-device");
            $("header").height(0);
            console.log("ios");
        }
        if ( windowHeight / windowWidth > 1.625 && windowWidth > 700 && windowHeight > 1000 ) {
            $("#mobile-wrapper").css({
                "position": "relative",
                "top": "10%"
            });
        } else if ( windowHeight / windowWidth > 1.625 && windowWidth >= 600 && windowHeight > 1000 ) {
            $("#mobile-wrapper").css({
                "position": "relative",
                "top": "5%"
            });
        }
    }

    function checkScreenSize() {
        console.log('check screen size started');
        mainJS();
    }

    $(window).resize(function() {
        checkLandscape();
    });
    $(document).ready(function() {
        console.log("document ready");
        checkScreenSize();
        checkLandscape();
        $("html, body").animate({ scrollLeft: 0 }, "0");
    });
})(jQuery);

var video_url_desktop = jQuery("#modalVideo iframe").attr("src");

function modalVideo() {
    jQuery("#modalVideo").slideDown("200");
    jQuery(".modalBackground").fadeIn("200");
    jQuery("#modalVideo iframe").attr("src", video_url_desktop);
    jQuery("#modalVideo iframe")[ 0 ].src += "&autoplay=1";
}

// Animation Boards on click
var firstBoard = jQuery("#first-boar");
var secondBoard = jQuery("#second-board");
var thirdBoard = jQuery("#third-board");
var fourthBoard = jQuery("#fourth-board");

var twist = new TimelineMax({
    paused: true
});
var twistTitle = new TimelineMax({
    paused: true
});
twist.clear();
function firstBoardClicked() {
    var firstBoardTitle = jQuery("#register-child");
    twist.to(firstBoard, 4, {
        rotationY: 900,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twistTitle.to(firstBoardTitle, 4, {
        rotationY: 1080,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twist.play();
    twistTitle.play();
    ga('send', 'event', 'Сhild', 'Record');
}

function secondBoardClicked() {
    var secondBoardTitle = jQuery("#closest-school");
    twist.to(secondBoard, 4, {
        rotationY: 900,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twistTitle.to(secondBoardTitle, 4, {
        rotationY: 1080,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twist.play();
    twistTitle.play();
    ga('send', 'event', 'SchoolNear', 'Find');
}

function thirdBoardClicked() {
    var thirdBoardTitle = jQuery("#check-schedule");
    var thirdBoardItems = [ thirdBoard, thirdBoardTitle ];
    twist.to(thirdBoardItems, 4, {
        rotationY: 900,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twistTitle.to(thirdBoardTitle, 4, {
        rotationY: 1080,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twist.play();
    twistTitle.play();
    ga('send', 'event', 'View', 'Schedule');
}

function fourthBoardClicked() {
    var fourthBoardTitle = jQuery("#trial-month");
    var fourthBoardItems = [ fourthBoard, fourthBoardTitle ];
    twist.to(fourthBoardItems, 4, {
        rotationY: 900,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twistTitle.to(fourthBoardTitle, 4, {
        rotationY: 1080,
        transformOrigin: "center center",
        ease: Elastic.easeOut
    });
    twist.play();
    twistTitle.play();
    ga('send', 'event', 'TrialLesson', 'ClickLesson');
}
// Регулировка скорости и продолжительности
(function($){
    $('#testScrollTo').on('click', function(){
        $('#testScrollTo').attr('disabled', 'true');
        console.info('scrollToEnd animation started!');

        $('html, body').animate({
            scrollLeft: parseInt(parseInt($('#main-wrapper').width())*0.6031233171782445)+100
        }, {
            duration: 15000,
            specialEasing: {
                scrollLeft: "linear"
            },
            complete: function() {
                console.error('scrollToEnd animation stopped!');
                $('#testScrollTo').removeAttr('disabled');
            }
        });
    });
})(jQuery);

// console.log(document.querySelectorAll('.sign-block.not-logged iframe')[0].contentWindow);