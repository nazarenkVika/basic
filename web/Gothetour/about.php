<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>О компании</title>
	<link rel="stylesheet" href="../wp-content/themes/css/color-shemes/yellow-orange/yellow-orange.css" type="text/css">
	<link rel="stylesheet" href="index2.html" type="text/css">
	<link rel="shortcut icon" href="../wp-content/themes/images/favicon.png" type="image/x-icon"/>
	<link rel="pingback" href="../xmlrpc.html"/>
	<link href="../wp-content/uploads/bootstrap/bootstrap.min.css" rel="stylesheet">
</head>
<body class="page-template page-template-template-page-history page-template-template-page-history-php page page-id-3595">

	<link rel="stylesheet" href="../wp-content/themes/style68b3.css?ver=1" type="text/css">
	<header>
		<div id="header">
		<div class="wrap">
			<div class="button-group">
				<div class="call-phone" style="margin-left: 45px; margin-top: 50px;">
					<a href="tel:+375259033153"></a>
				</div>
			</div>
			<div class="cloud">
				<div class="menu">
					<a class="navicon mtoggle" href="#">
					Меню
					</a>	
				</div>
			</div>
			<div id="logo-container" class="logo">
				<a id="logo" href="../index.html">
				<img src="../wp-content/themes/images/backgrounds/Logo.png" >
				</a>
			</div>
			<div class="menu">
				<div id="main-navigation" class="menu-mani-menu-container" style="margin-left: 280px;">
					<ul id="menu-mani-menu" class="dd-menu">
						<li id="menu-item-2619" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2619"><a href="../bron/index.html">Бронь</a></li>
						<li id="menu-item-2620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2620">
							<a href="#">О нас</a>
							<ul class="sub-menu">
								<li id="menu-item-3597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3597"><a title="Ментальная арифметика" href="../about/index.html">О компании INTHETOUR</a></li>
								<li id="menu-item-2622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2622"><a href="../blog/index.html">Блог</a></li>
							</ul>
						</li>
						<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065"><a href="../reviews/index.html">Отзывы</a>
						</li>
						<li id="menu-item-2691" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2691"><a href="../contacts/index.html">Контакты</a></li>
						<li id="menu-item-3907" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3907"><a href="../hotels/index.html">Отели</a></li>
					</ul>
				</div>
			</div>
			<style type="text/css">
				.qtranxs_widget { display: none; }
				.qtranxs_widget ul { z-index:99999999; margin: 0; }
				.qtranxs_widget ul li
				{
				display: inline; 
				list-style-type: none; 
				margin: 0 5px 0 0; 
				opacity: 0.5;
				-o-transition: 1s ease opacity;
				-moz-transition: 1s ease opacity;
				-webkit-transition: 1s ease opacity;
				transition: 1s ease opacity;
				}
				.qtranxs_widget ul li span { margin: 0 5px 0 0; }  /* other way to control spacing */
				.qtranxs_widget ul li.active { opacity: 0.8; }
				.qtranxs_widget ul li:hover { opacity: 1; }
				.qtranxs_widget img { box-shadow: none; vertical-align: middle; display: initial; }
				.qtranxs_flag { height:12px; width:18px; display:block; }
				.qtranxs_flag_and_text { padding-left:20px; }
				.qtranxs_flag span { display:none; }
			</style>
			<div id="qtranslate-2" class="widget qtranxs_widget">
				<ul class="language-chooser language-chooser-custom qtranxs_language_chooser" id="qtranslate-2-chooser">
					<li class="language-chooser-item language-chooser-item-ru active"><a href="index.html" title="Русский (ru)">ru</a></li>
				</ul>
				<div class="qtranxs_widget_end"></div>
			</div>
		</div>
	</header>
	<div id="intro">
		<div id="container-middle-top-edge"></div>
		<div id="container-middle-bottom-edge"></div>
	</div>
	<div id="content" class="franshiza">
	<div class="wrap">
	<div class="container">
	<div class="row">
	<div class="col-xs-12">
	<div class="page page-content">
	<div class="franshiza-text" >
	<h1 style="font-family: 'Comfortaa', cursive;margin-left: 450px; color: orange;" class="title" style="width: 500px;">О компании</h1><br>
	<h4 style="width: 800px; margin-left:170px; text-align: center;">Туристическая компания «INTHETOUR» создана в 2018 году. Целью создания компании было оказание туристических услуг населению. Все это время компания «INTHETOUR» успешно работает на рынке туристических услуг и предлагает населению как отдых, так и экскурсионные туры. Среди туров предлагаемых нашей компанией каждый желающий может выбрать именно свой тур , а наши специалисты помогут и окажут необходимую консультационную помощь в подборе тура. Для этой цели мы разрабатываем и предлагаем на наш рынок такие направления для отдыха как Турция, Египет, Кипр, Греция, Испания, Болгария, Черногория, Хорватия, ОАЭ, Таиланд, Куба, Доминикана, Мальдивы, Индонезия, Гоа, Малайзия,  туры в  Крым, Украину, Литву, Латвию, Польшу, а так же обширную экскурсионную программу по странам Восточной и Западной Европы. 
	Кроме того, мы активно принимаем участие в разработке и продвижении на международном рынке туристических услуг прием и обслуживание в Беларуси. Для этой цели наши специалисты разрабатывают экскурсионные программы, которые мы предлагаем как для групп, так и для индивидуальных туристов.
	«INTHETOUR» - это компания, которая рада каждому туристу и не оставит его без внимания, которая имеет немало постоянных клиентов и партнеров и будет рада каждому туристу, который решит обратиться к нашим специалистам.
</h4><br> 

<h2 style="font-family: 'Comfortaa', cursive; text-align:center;margin-left:50px;">Видео о турфирме "IN THE TOUR"</h2>
<br>


<p style="font-family: 'Comfortaa', cursive;text-align:center; margin-left: 60px;"><iframe width="560" height="315" src="https://www.youtube.com/embed/ZUHrr7EYWPk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<!--<div ><iframe  width="560" margin-left="300" height="315" src="https://www.youtube.com/embed/ZUHrr7EYWPk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
--></script>
</body>
</html>