$('.open_button').click(function () {

	var timeMe;
	var finalCountDown;

	var completed = 1
	var totalpanels = $('#quiz .panel').length - 1;
	progresscalc();

	$('.nextbutton').click(function () {
		if (!$(this).is("#calc")) {
			$('.panel.current').removeClass('current').addClass('completed').next('.panel').addClass('current');
			completed++;
			progresscalc();
		}
	})

	$('.lastbutton').click(function () {
		$('.panel.current').removeClass('current').prev('.panel.completed').removeClass('completed').addClass('current');
		completed--;
		progresscalc();
	})

	$('.retake').click(function () {
		restart();
	})

	var possible_results = [
		// {
		// 	name: "Название тура",
		// 	price: 10,
		// 	fun: 10,
		// 	countPlace: 5,
		// 	duration: 10,
		// 	rating: 5,
		// 	similarity: 0,
		// 	desc: "Описание тура",
		// 	img: "url",
		// },
		{
			id: 0,
			name: "тур \"Березинский заповедник\"",
			price: 4,
			fun: 3,
			countPlace: 1,
			duration: 1,
			rating: 5,
			similarity: 0,
			desc: "Для любителей природы и активного отдыха",
			img: "../Quiz/img/berezinski.jpg",
		},{
			id: 1,
			name: "тур \"Браславские озера + Париж\"",
			price: 8,
			fun: 3,
			countPlace: 2,
			duration: 2,
			rating: 4,
			similarity: 0,
			desc: "Да, на маршруте не попадётся Эйфелева башня, Язык Тролля или амстердамские кофешопы, но зато ты увидишь самые-самые красивые родные леса, поля и, конечно, озёра.",
			img: "../Quiz/img/braslav.jpg",
		},{
			id: 3,
			name: "тур \"Дукорски маёнтак\"",
			price: 6,
			fun: 4,
			countPlace: 1,
			duration: 1,
			rating: 3,
			similarity: 0,
			desc: "Для ценителей истории :)",
			img: "../Quiz/img/maentak.jpg",
		},{
			id: 4,
			name: "тур \"Обзорная экскурсия по Минску\"",
			price: 2,
			fun: 1,
			countPlace: 5,
			duration: 1,
			rating: 1,
			similarity: 0,
			desc: "Для тех, кто любит Минск, но не всё знает о нём",
			img: "../Quiz/img/minsk.jpg",
		},{
			id: 5,
			name: "тур \"Дудутки\"",
			price: 5,
			fun: 2,
			countPlace: 1,
			duration: 2,
			rating: 2,
			similarity: 0,
			desc: "Для любителей новых знаний и природы :)",
			img: "../Quiz/img/dudutki.jpg",
		},{
			id: 6,
			name: "тур \"Топ-4 замка Беларуси\"",
			price: 10,
			fun: 5,
			countPlace: 4,
			duration: 4,
			rating: 5,
			similarity: 0,
			desc: "Для тех, кто так и не увидел прекрасные памятники архитектуры РБ :)",
			img: "../Quiz/img/top-4.jpg",
		},{
			id: 7,
			name: "тур \"Гродо + культовые места\"",
			price: 9,
			fun: 4,
			countPlace: 4,
			duration: 3,
			rating: 4,
			similarity: 0,
			desc: "Для желающих узнать Беларусь с другой стороны",
			img: "../Quiz/img/grodno.jpg",
		},
	],
		me = {
			price: 0,
			fun: 0,
			countPlace: 0,
			duration: 0,
			rating: 0
		};

	$('#quiz input:radio').change(function () {
		$(this).siblings('.nextbutton').addClass('active');
		this_value = $(this).val(),
			this_aspect = $(this).attr('name');
		me[this_aspect] = this_value;
	});

	$('#calc').click(function () {
		comparison();
		$('.panel.current').removeClass('current').addClass('completed').next('.panel').addClass('current');
	});

	function comparison() {
		var winner = "", winning_score = "1000";
		for (i = 0; i < possible_results.length; i++) {
			direct_comparison(i, 'price');
			direct_comparison(i, 'fun');
			direct_comparison(i, 'countPlace');
			direct_comparison(i, 'duration');
			direct_comparison(i, 'rating');
		}
		possible_results.sort((a, b) => a['similarity'] > b['similarity'] ? 1 : -1);
		winning_score = possible_results[0]['similarity'];

		$('#result').empty().append(possible_results[0]['name']);
		$('.resultspanel #desc').empty().append(possible_results[0]['desc']);
		$('.resultspanel #finalimage').attr('style', 'background:url("' + possible_results[0]['img'] + '") 50% 50%; background-size:cover; ');
	};

	function direct_comparison(number, aspect) {
		possible_results[number]['similarity'] += Math.abs(possible_results[number][aspect] - me[aspect]);
	}

	function progresscalc() {
		$('.progress').remove();
		if (completed <= totalpanels) {
			$('.panel.current .question').prepend('<div class="progress">' + (completed - 1) + ' из ' + (totalpanels - 1) + '</div>');
		}
	}

	function restart() {
		$('.panel').each(function () {
			$(this).removeClass('current').removeClass('completed');
		})

		$('.startpanel').addClass('current');

		$('input').each(function () {
			$(this).prop('checked', false);
		})

		$('.nextbutton').each(function () {
			$(this).removeClass('active');
		})

		completed = 1;

		me[price] = 0;
		me[fun] = 0;
		me[countPlace] = 0;
		me[duration] = 0;
		me[rating] = 0;
	}


});