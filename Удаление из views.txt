tour-------

    <?= $form->field($model, 'gallery_id')->textInput() ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'viewed')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'rating_id')->textInput() ?>

    <?= $form->field($model, 'tour_type')->textInput(['maxlength' => true]) ?> 