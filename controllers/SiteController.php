<?php

namespace app\controllers;

use app\models\Tour;
use app\models\Category;
use app\models\Reservation;
use app\models\ReservationForm;
use app\models\CommentForm;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ReviewsForm;
use app\models\SearchForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Reviews;
use yii\db\ActiveQuery;
use yii\web\UrlManager;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

public function beforeAction($action){
    $model = new SearchForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()){
        $q = Html::encode($model->q);
        return $this->redirect(Yii::$app->urlManager->createUrl(['site/search', 'q' => $q]));
    }
    return true;
}


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
   
    public function actionTours(){
/*создаем сначала копию модели и только после этого можем вызвать ее и сделать в модели статичную модель*/
        
        $data = Tour::getAll(1);
        $popular = Tour::getPopular(); 
        $recent = Tour::getRecent(); 
        $categories = Category::getAll();

        $query = Tour::find();
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count,'pageSize'=>3]);
        //$pagination=new Pagination(['totalCount'=>$query->count()]);
        $tours = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
     
        return $this->render('tours',[
            'tours'=>$tours,
            'pagination'=>$pagination,
            'popular'=>$popular,
            'recent'=>$recent,
            'categories'=>$categories,

        ]);
    }

    public function actionSingletour($id){

        $tour=Tour::findOne($id);
        $popular = Tour::getPopular(); 
        $recent = Tour::getRecent(); 
        $categories = Category::getAll();
        $comments = $tour->getTourComments();
        $commentForm = new CommentForm();
        $reserevationForm = new ReservationForm();
        $reservation = Reservation::getActiveReservation($id);

        return $this->render('singletour',[
        'tour'=>$tour,
        'popular'=>$popular,
        'recent'=>$recent,
        'categories'=>$categories,
        'comments'=>$comments,
        'commentForm'=>$commentForm,
        'reserevationForm'=>$reserevationForm,
        'reservation'=>$reservation,
        ]);
    }

    public function actionCategory($id){



        $data = Category::getTourByCategory($id);
        $popular = Tour::getPopular(); 
        $recent = Tour::getRecent(); 
        $categories = Category::getAll();

        return $this->render('category',[
            'tours'=>$data['tours'],
            'pagination'=>$data['pagination'],
            'popular'=>$popular,
            'recent'=>$recent,
            'categories'=>$categories,
        ]);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['tour_id'=>'id']);
    }

    public function actionComment($id)
    {
        $model = new CommentForm();
        
        if(Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if($model->saveComment($id))
            {
                Yii::$app->getSession()->setFlash('comment', 'Ваш комментарий успешно оставлен и ожидает модерацию!');
                return $this->redirect(['site/singletour','id'=>$id]);
            }
        }
    }

    public function actionReserve($id)
    {
        $model = new ReservationForm();
        
        if(Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if($model->saveReservation($id))
            {
                Yii::$app->getSession()->setFlash('comment', 'Забронировано!');
                return $this->redirect(['site/singletour','id'=>$id]);
            }
        }
    }

    // public function actionReviews(){
    //    // $comments = $tour->getTourComments();
    //     $reviews= Reviews::find()->all();  
    //     $reviewsForm= new ReviewsForm();   

    //     return $this->render('reviews',[
    //     'reviews'=>$reviews,
    //     'reviewsForm'=>$reviewsForm,
    //         ]);

    // }
    
    public function actionReviews(){

         $model = new ReviewsForm();

         if(Yii::$app->request->isPost){
             $model->load(Yii::$app->request->post());
             if($model->saveReviews())
             {
                 Yii::$app->getSession()->setFlash('review', 'Ваш отзыв успешно оставлен и ожидает модерацию!');
                 return $this->redirect(['site/reviews']);
             }
         }
        
         //$reviews = $reviews->where(['status'=>1])->all();
         $reviews= Reviews::find()->where(['status'=>1])->all();  
         $reviewsForm= new ReviewsForm();   
 
         return $this->render('reviews',[
         'reviews'=>$reviews,
         'reviewsForm'=>$reviewsForm,
             ]);
     }

     public function actionSearch($pageSize = 5){

        $popular = Tour::find()->orderBy('viewed desc')->limit(3)->all(); 

        $recent = Tour::find()->orderBy('date asc')->limit(4)->all(); 

        $categories = Category::getAll();


        $search = Yii::$app->request->get('search');

        $search1 = str_replace(' ','',$search);

        $query = Tour::find()->where(['like', 'replace (title, " ", "")',$search1]);

        $count = $query->count();

        $pagination = new Pagination(['totalCount' => $count,'pageSize'=>$pageSize]);
        
        $tours = $query->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();

        return $this->render('tours',[
            'tours'=>$tours,
            'pagination'=>$pagination,
            'popular'=>$popular,
            'recent'=>$recent,
            'categories'=>$categories,
        ]);
            
            return $this->render('tours',[
                'tours'=>$tours,
                'pagination'=>$pagination,
                'popular'=>$popular,
                'recent'=>$recent,
                'categories'=>$categories,
            ]);
    }

 public function actionFeedback(){
     return $this->render('feedback');
 }
}
