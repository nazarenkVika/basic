<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tour_tag}}`.
 */
class m200412_185504_create_tour_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tour_tag}}', [
            'id' => $this->primaryKey(),
            'tour_id'=>$this->integer(),
            'tag_id'=>$this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tour_tag}}');
    }
}
