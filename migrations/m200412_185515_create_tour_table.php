<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tour}}`.
 */
class m200412_185515_create_tour_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tour}}', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(),
            'description'=>$this->text(),
            'content'=>$this->text(),
            'price'=>$this->double(),
            'continent'=> $this->string(),
            'date'=>$this->date(),
            'duration'=>$this->integer(),
            'gallery_id'=>$this->integer(),
            'image'=>$this->string(),
            'viewed'=>$this->integer(),
            'user_id'=>$this->integer(),
            'status'=>$this->integer(),
            'category_id'=>$this->integer(),
            'rating_id'=>$this->integer(),
            'tour_type'=>$this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tour}}');
    }
}
