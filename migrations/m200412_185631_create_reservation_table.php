<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%reservation}}`.
 */
class m200412_185631_create_reservation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%reservation}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'tour_id'=>$this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%reservation}}');
    }
}
