<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%reservation}}`.
 */
class m200510_154447_add_status_column_to_reservation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%reservation}}', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%reservation}}', 'status');
    }
}
