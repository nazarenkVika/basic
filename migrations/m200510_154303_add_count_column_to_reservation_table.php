<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%reservation}}`.
 */
class m200510_154303_add_count_column_to_reservation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%reservation}}', 'count', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%reservation}}', 'count');
    }
}
