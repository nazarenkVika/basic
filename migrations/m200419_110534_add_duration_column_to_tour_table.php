<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tour}}`.
 */
class m200419_110534_add_duration_column_to_tour_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tour}}', 'duration', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tour}}', 'duration');
    }
}
