<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Tour;
use app\models\Gallery;
use app\models\TourSearch;
use app\models\ImageUpload;
use app\models\GalleryUpload;
use app\models\Category;
use app\models\Tag;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TourController implements the CRUD actions for Tour model.
 */
class TourController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tour models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tour model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tour model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tour();

        if ($model->load(Yii::$app->request->post()) && $model->saveTour()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tour model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->saveTour()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tour model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tour::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionSetImage($id)
    {//создается экземплям модели по загрузке картинки
        $model=new ImageUpload;

        if(Yii::$app->request->isPost){
            $tour=$this->findMOdel($id);
        
            $file=UploadedFile::getInstance($model,'image');
            
            if($tour->saveImage($model->uploadFile($file, $tour->image))){

                return $this->redirect(['view', 'id'=>$tour->id]);

            }
        }

    return $this->render('image',['model'=>$model]);
    }

    public function actionSetGallery($id){
        $model=new GalleryUpload;

        if(Yii::$app->request->isPost){
            $tour=$this->findMOdel($id);
            $files=UploadedFile::getInstances($model,'images');
            $gallery = new Gallery();
            if($gallery->saveGallery($id, $model->uploadFiles($files))){
                return $this->redirect(['view', 'id'=>$tour->id]);
            }
        }

    return $this->render('gallery',['model'=>$model]);
    }

    public function actionSetCategory($id)
    {
        
        $tour = $this->findModel($id);
        $selectedCategory = ($tour->category) ? $tour->category->id : '0';
        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'title');

        if(Yii::$app->request->isPost)
        {
            $category = Yii::$app->request->post('category');
            if($tour->saveCategory($category))
            {
                return $this->redirect(['view', 'id'=>$tour->id]);
            }
        }

        return $this->render('category', [
            'tour'=>$tour,
            'selectedCategory'=>$selectedCategory,
            'categories'=>$categories
        ]);
    }
    public function actionSetTags($id)
    {
        $tour = $this->findModel($id);
        $selectedTags = $tour->getSelectedTags(); 
        //$selectedTags = ($tour->tags) ? $tour->tags->id : '0';
        $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title');

        if(Yii::$app->request->isPost)
        {
            $tags = Yii::$app->request->post('tags');
            $tour->saveTags($tags);
            return $this->redirect(['view', 'id'=>$tour->id]);
        }
        
        return $this->render('tags', [
            'selectedTags'=>$selectedTags,
            'tags'=>$tags
        ]);
    }
}


