

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(!empty($reviews)):?>

        <table class="table">
            <thead>
                <tr>
                    <td>id</td>
                    <td>title</td>
                    <td>text</td>
                    <td>user_id</td>
                    <td>Action</td>
                </tr>
            </thead>
           
            <tbody>

            <?php foreach($reviews as $review):?>
                <tr>
                    <td><?=$review->id?></td>
                    <td><?=$review->title?></td>
                    <td><?=$review->text?></td>
                    <td><?=$review->user->name?></td>
                    <td>
                        <?php if($review->isAllowed()):?>
                            <a class="btn btn-warning" href="<?=Url::toRoute(['reviews/disallow','id'=>$review->id]);?>">Disallow</a>
                        <?php else:?>
                            <a class="btn btn-success" href="<?=Url::toRoute(['reviews/allow','id'=>$review->id]);?>">Allow</a>
                        <?php endif;?>
                        <a class="btn btn-danger" href="<?=Url::toRoute(['reviews/delete','id'=>$review->id]);?>">Delete</a>
                    </td>
                </tr>
            <?php endforeach;?>

        </tbody> 
        </table>   
    <?php endif;?>


</div>
