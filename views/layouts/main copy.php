<?php
	/* @var $this \yii\web\View */
	/* @var $content string */
	use app\assets\PublicAsset;
	use app\widgets\Alert;
	use yii\helpers\Html;
	use yii\bootstrap\Nav;
	use yii\bootstrap\NavBar;
	use yii\widgets\Breadcrumbs;
	use app\assets\AppAsset;
	use yii\helpers\Url;
	use app\models\SearchForm;
	use yii\widgets\ActiveForm;
	PublicAsset::register($this);

	$model = new SearchForm();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php $this->registerCsrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<title>
			Турагентство INTHETOUR
		</title>
		<link rel="shortcut icon" href="../Gothetour/images/favicon.png" type="image/x-icon"/>
		<?php $this->head() ?>
			<!-- <link rel="stylesheet" href="../Gothetour/css/style68b3.css?ver=1" type="text/css"> -->
<style>
form {
  position: relative;
  width: 300px;
  margin: 0 auto;
}
input {
  width: 70%;
  height: 32px;
  padding-left: 10px;
  border: 2px solid #7BA7AB;
  border-radius: 5px;
  outline: none;
  background: #F9F0DA;
  color: #9E9C9C;
 margin-top:27px;
 z-index:501;
}
</style>
	</head>
	<body>
		<?php $this->beginBody() ?>
		<body class="home blog">
			<header>
				<div id="header">
					<div class="wrap">
						<!-- Search -->

						<div class="button-group">
						<section class="box_search" style="margin-left:-200px; margin-top:-30px;">
											<form method="get" action="<?= Url::to(['site/search'])?>">
												<input type="text" class="text" name="search" placeholder="Поиск по турам"/>
											</form>
										</section>
							<div class="call-phone" style="margin-left: 45px; margin-top: -45px;">
								<a href="tel:+375259033153;"></a>
							</div>
						</div>
						<div class="cloud">
							<div class="menu">
								<a class="navicon mtoggle" href="#">
								Меню
								</a>	
							</div>
						</div>
						<div id="logo-container" class="logo" style="margin-right:800px;">
							<a id="logo" href="../site/index">
							<img src="../Gothetour/images/backgrounds/Logo.png" >
							</a>
						</div>
						<div class="menu">
							<div id="main-navigation" class="menu-mani-menu-container" style="margin-left: 280px;">
								<ul style="margin-right:50px;" id="menu-mani-menu" class="dd-menu">
									<li id="menu-item-2619" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2619"><a href="../site/reserved">Бронь</a></li>
									<li id="menu-item-2620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2620">
										<a href="../site/about">О нас</a>
										<!-- <ul class="sub-menu">
											<li id="menu-item-3597" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3597"><a title="title" href="about/index.html">О компании GOTHETOUR</a></li>
											<li id="menu-item-2622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2622"><a href="blog/index.html">Блог</a></li>
										</ul> -->
									</li>
									<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065">
										<a href="../site/reviews">Отзывы</a>
									</li>
									<li id="menu-item-2691" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2691"><a href="../site/contact">Контакты</a></li>
									<li id="menu-item-3907" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3907"><a href="tours">Туры</a></li>
									<?php if(Yii::$app->user->isGuest):?>
									<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065">
										<a href="<?= Url::toRoute(['/auth/login'])?>">Вход</a>
									</li>
									<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065">
										<a href="<?= Url::toRoute(['/auth/signup'])?>">Регистрация</a>
									</li>
									<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065">
									<?php else: ?>
										<?= Html::beginForm(['/auth/logout'], 'post')
										. Html::submitButton(
											'Выход (' . Yii::$app->user->identity->name . ')',
											['id' => 'menu-item-3065', 'class'=>"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065",'style'=>"padding-top:7px;margin-left:-27px;background:none; font-family: Comforta Bold, Arial, sans-serif;color:#15974c;cursor:pointer;    font-size: 18px; border: none; outline: none;"]
										)
										. Html::endForm() ?>
									<?php endif;?>
										</li>
									<!-- <li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3065">
										<a href="<?= Url::toRoute(['/auth/logout'])?>">Выход</a>
									</li> -->

								</ul>
							</div>
						</div>
					</div>
				</div>
			</header>


			<?= $content ?>


			<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>