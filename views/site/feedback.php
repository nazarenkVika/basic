<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <?php $this->title = 'Бронирование'?>
   <link rel="shortcut icon" href="../wp-content/themes/images/favicon.png" type="image/x-icon"/>
   <!-- Emailjs.com -->
   <script type="text/javascript" src="https://cdn.emailjs.com/sdk/2.3.2/email.min.js"></script>
   <script type="text/javascript">
      (function(){
         emailjs.init("user_XQNwe8FwJmjwuicDiQjZk");
      })();
   </script>
   <script type="text/javascript">
      window.onload = function() {
        console.log("init")
          document.getElementById('contact-form').addEventListener('submit', function(event) {
              event.preventDefault();  
              var template_params = {
                "name": "name_value",
                "phone": "phone_value",
                "message": "message_value"
              }
      
              var service_id = "default_service";
              var template_id = "form_templates";
      
              emailjs.sendForm(service_id, template_id, this);
              alert("Сообщение отправлено")
          });
      }
   </script>
   <!--  -->
   <!--отображение -->
   <script type='text/javascript' src='../wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
   <script type='text/javascript' src='../wp-content/themes/slick/slick68b3.js?ver=1'></script>
   <style>
      input_text{
      border: 1px solid #cccccc; 
      border-radius: 3px; 
      -webkit-border-radius: 3px; 
      -moz-border-radius: 3px; 
      -khtml-border-radius: 3px; 
      background: #ffffff !important; 
      outline: none; 
      height: 24px; 
      width: 120px;
      color: #cccccc; 
      font-size: 11px; 
      font-family: Tahoma;
      }
      @import url('https://fonts.googleapis.com/css?family=Dosis');
      :root {
      /* generic */
      --gutterSm: 0.4rem;
      --gutterMd: 0.8rem;
      --gutterLg: 1.6rem;
      --gutterXl: 2.4rem;
      --gutterXx: 7.2rem;
      --colorPrimary400: #ee330e;
      --colorPrimary600: #ee330e;
      --colorPrimary800: #ee330e;
      --fontFamily: "Dosis", sans-serif;
      --fontSizeSm: 1.2rem;
      --fontSizeMd: 1.6rem;
      --fontSizeLg: 2.1rem;
      --fontSizeXl: 2.8rem;
      --fontSizeXx: 3.6rem;
      --lineHeightSm: 1.1;
      --lineHeightMd: 1.8;
      --transitionDuration: 300ms;
      --transitionTF: cubic-bezier(0.645, 0.045, 0.355, 1);
      /* floated labels */
      --inputPaddingV: var(--gutterMd);
      --inputPaddingH: var(--gutterLg);
      --inputFontSize: var(--fontSizeLg);
      --inputLineHeight: var(--lineHeightMd);
      --labelScaleFactor: 0.8;
      --labelDefaultPosY: 50%;
      --labelTransformedPosY: calc(
      (var(--labelDefaultPosY)) - 
      (var(--inputPaddingV) * var(--labelScaleFactor)) - 
      (var(--inputFontSize) * var(--inputLineHeight))
      );
      --inputTransitionDuration: var(--transitionDuration);
      --inputTransitionTF: var(--transitionTF);
      }
      *,
      *::before,
      *::after {
      box-sizing: border-box;
      }
      html {
      font-size: 10px;
      }
      body {
      display: flex;
      align-items: center;
      justify-content: center;
      overflow: hidden;
      width: 100vw;
      height: 50vh;
      color: #455A64;
      background-color: rgb(194, 87, 110);
      font-family: var(--fontFamily);
      font-size: var(--fontSizeMd);
      line-height: var(--lineHeightMd);
      border-radius: 10px;
      }
      .Wrapper {
      flex: 0 0 80%;
      max-width: 80%;
      }
      .Title {
      margin: 0 0 var(--gutterXx) 0;
      padding: 0;
      color: #fff;
      font-size: var(--fontSizeXx);
      font-weight: 400;
      line-height: var(--lineHeightSm);
      text-align: center;
      text-shadow: -0.1rem 0.1rem 0.2rem var(--colorPrimary800);
      border-radius: 10px;
      }
      .Input {
      position: relative;
      }
      .Input-text {
      display: block;
      margin: 0;
      padding: var(--inputPaddingV) var(--inputPaddingH);
      color: inherit;
      width: 100%;
      font-family: inherit;
      font-size: var(--inputFontSize);
      font-weight: inherit;
      border-radius: 10px;
      /* line-height: var(--inputLineHeight); */
      border: none;
      border-radius: 0.9rem;
      transition: box-shadow var(--transitionDuration);
      }
      .Input-text::placeholder {
      color: #B0BEC5;
      }
      .Input-text:focus {
      outline: none;
      box-shadow: 0.2rem 0.8rem 1.6rem var(--colorPrimary600);
      }
      .Input-label {
      border-radius: 10px;
      display: block;
      position: absolute;
      bottom: 50%;
      left: 1rem;
      color: #fff;
      font-family: inherit;
      font-size: var(--inputFontSize);
      font-weight: inherit;
      line-height: var(--inputLineHeight);
      opacity: 0;
      transform: 
      translate3d(0, var(--labelDefaultPosY), 0)
      scale(1);
      transform-origin: 0 0;
      transition:
      opacity var(--inputTransitionDuration) var(--inputTransitionTF),
      transform var(--inputTransitionDuration) var(--inputTransitionTF),
      visibility 0ms var(--inputTransitionDuration) var(--inputTransitionTF),
      z-index 0ms var(--inputTransitionDuration) var(--inputTransitionTF);
      }
      .Input-text:placeholder-shown + .Input-label {
      visibility: hidden;
      z-index: -1;
      }
      .Input-text:not(:placeholder-shown) + .Input-label,
      .Input-text:focus:not(:placeholder-shown) + .Input-label {
      visibility: visible;
      z-index: 1;
      opacity: 1;
      transform:
      translate3d(0, var(--labelTransformedPosY), 0)
      scale(var(--labelScaleFactor));
      transition:
      transform var(--inputTransitionDuration),
      visibility 0ms,
      z-index 0ms;
      }
   </style>
</head>
<body class="home blog">
   <!-- Логотип -->
   <!-- <div id="logo-container" style="z-index: 999999;" class="logo">
      <a id="logo" href="../site/index">
      <img src="../Gothetour/images/backgrounds/Logo.png" >
      </a>
   </div> -->
  
   <div id="footer-form"  class="left-block col-md-5 col-sm-5 " style=" Margin-top:300px;  ">
      <div class="col-md-8 col-md-offset-2">
         <div role="form" class="wpcf7" id="wpcf7-f1957-o1" dir="ltr">
            <div class="screen-reader-response">              <div id="content"> <h1 class="block-title" style="margin-left:10px;margin-top:200px;margin-bottom:-100px;">Форма обратной связи</h1></div></div>
            <!-- Bаck панели -->
            <div class="input_text" style="display:block;background-color:#ffb950;  width: 400px;height: 380px;  border-radius: 10px;" >
               <p >
                   
                  <span class="wpcf7-form-control-wrap name">
                     <span class="wpcf7-form-control-wrap phone">
               <div class="Wrapper" >


               <!-- Форма, откуда берутся значения для EmailJs -->
               <form id="contact-form"> 
               <div class="Input" style="Margin-left:80px;Margin-top:130px;padding-top: 20px;     border-radius: 10px;">
               <!-- Валидация Input -->
               <input type="text" id="input" class="Input-text"  name="name" required  pattern="^\[A-Za-zА-Яа-яЁё]$" onkeyup="var yratext=/['0-9',':']/; if(yratext.test(this.value)) alert('Введены запрещенные символы')" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="name1" aria-required="true" aria-invalid="false" placeholder="Ваше имя" /></span><br />
               <input type="text" id="input" class="Input-text"  type="tel" required pattern="^\+375\d{2}\d{7}$" name="phone" value="" size="40"  class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="phone1" aria-required="true" aria-invalid="false" placeholder="Ваш телефон" />
               <br />
               <textarea  id="input" class="Input-text"  name="message" cols="40" rows="7" class="wpcf7-form-control wpcf7-textarea" id="message1" aria-invalid="false" placeholder="Текст сообщения (не обязательно)"></textarea></span><br />
               </div>
               </div>
               <span class="wpcf7-form-control-wrap message">
               </p>
            </div>
            <div class="container-message"></div>
            <p> <input type="submit" value="Заказать звонок" class="wpcf7-form-control wpcf7-submit" /> </p>
            </form>
         </div>
      </div>
   </div>
   <link rel="stylesheet" href="../Gothetour/css/style68b3.css?ver=1" type="text/css">
   <!-- <header>
      <div id="header">
         <div class="wrap">
            <div class="button-group">
               <div class="call-phone" style="margin-left: 45px; margin-top: 50px;">
                  <a href="tel:+375259033153;"></a>
               </div>
            </div>
            <div class="cloud">
               <div class="menu">
                  <a class="navicon mtoggle" href="#">
                  Меню
                  </a>	
               </div>
            </div>
         </div>
      </div>
   </header> -->
   </div>
   <!--Background1-->
   <div class="horizon background" style="height: 140%; z-index: -10000; ">
      <img src="../Gothetour/images/backgrounds/background-bron.svg"/>
   </div>
   </section>
   <!-- EmailJs -->
   <script type="text/javascript" src="https://cdn.emailjs.com/sdk/2.3.2/email.min.js"></script>
   <script type="text/javascript">
      (function(){
         emailjs.init("user_XQNwe8FwJmjwuicDiQjZk");
      })();
   </script>
   <!-- Правильное отображение -->
   <script src="../wp-content/themes/js/animation-home.js"></script>
   <link src="https://code.jquery.com/jquery-3.4.1.min.js">
</body>