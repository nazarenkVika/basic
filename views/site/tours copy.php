<?php
   /* @var $this yii\web\View */
   use yii\widgets\LinkPager;
   use yii\helpers\Html;
   use yii\helpers\Url;
   
   $this->title = 'Tours';
   $this->params['breadcrumbs'][] = $this->title;
   ?>



<h1 class="block-title" style="margin-top:-30px; margin-left:300px;">Лучшие туры </h1>
<div class="site-tours">
   <nav class="navbar main-menu navbar-default">
      <div class="container">
         <div class="menu-content">
            <!-- Brand and toggle get grouped for better mobile display -->
            <!-- /.navbar-collapse -->
         </div>
      </div>
      <!-- /.container-fluid -->
   </nav>
   <!--main content start-->
   <link  rel="stylesheet" type="text/css" src="Gothetour/css/new/bootstrap.min.css" >

   <div class="main-content">
      <div class="container">
         <div class="row">
            <div class="col-md-8">
               <!--post-->
               <?php foreach($tours as $tour):?>
               <style type="text/css">
                  img.wp-smiley,
                  img.emoji {
                  display: inline !important;
                  border: none !important;
                  box-shadow: none !important;
                  margin: 0 .07em !important;
                  vertical-align: -0.1em !important;
                  background: none !important;
                  padding: 0 !important;
                  }
               </style>
               <div id="blog-background">
                  <div id="content">
                     <div class="c-12 divider">
                        <ul class="portfolio-menu chocolate gallery" style="margin-left: 150px; ">
                           <li class="c-12 big-list col-md-4" style="float: left;">
                              <div class="img"a href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id]);?>"><img src="<?= $tour->getImage();?>" alt=""></a>
                              </div>
                              <h3 class="title" style="margin-top:-30px;"><a  style="margin-top:-30px; margin-left:-10px; font-size:24px;" href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id]);?>"><?= $tour->title?></a></h3>
                              <h4 style="margin-top:0px;margin-bottom:50px;">
                                 <p> <?= $tour->description?></p>
                              </h4>
                              <h5 class="social-share-title pull-left text-capitalize" style="margin-top:-40px;"> <?= $tour->getDate();?></h5>
                              <h5 style="margin-top:-38px;margin-left:210px; "><p style="    margin-top: -23px; margin-left: 50px;"><a href="<?= Url::toRoute(['site/category', 'id'=>$tour->category->id]);?>"><?= $tour->category->title;?></a></p></h5>
                              
                              <div class="meta" style="margin-left:-130px;">
                                 <a class="read-more" href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id]);?>" >Подробнее</a>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      </article>
      <?php endforeach; ?>    
      <!--pagination-->        
      <?php 
         echo LinkPager::widget([
          'pagination' => $pagination,
          ]);
         ?>
   </div>
   <!-- вызов сайдбара -->
   <?= $this->render('/partials/sidebar',[
      'popular'=>$popular,
      'recent'=>$recent,
      'categories'=>$categories,
      ]);?>
</div>
</div>
</div>

</div>