<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=c6a2e15a-d955-4e16-bc00-637e6a67068a" type="text/javascript"></script>
    <style type="text/css">
        html, body {
            width: 100%;
            height: 95%;
            padding: 0;
            margin: 0;
            font-family: Sans-Serif;
        }

        #map {
            width: 100%;
            height: 90%;
        }

        .placemark {
            background-image: url('../img/small.svg');
            background-size: 100%;
            width: 48px;
            height: 64px;
            margin-left: -17px;
            margin-top: -17px;
            animation:cubic-bezier 2s infinite;
        }

        .active {
            background-image: url('../img/big.svg');
            width: 160px;
            height:94px;
            margin-left: 0px;
            margin-top: 0px;
        }

        @keyframes show-big-placemark {
            0% {
                transform: scale(0);
            }
            100% {
                transform: scale(1);
            }
        }

        @keyframes show-small-placemark {
            0% {
                transform: scale(1);
                background-image: url('../img/big.svg');
                width: 160px;
                height: 94px;
                margin-left: -30px;
                margin-top: -60px
            }
            100% {
                transform: scale(0);
                background-image: url('../img/big.svg');
            }
        }

        @keyframes cubic-bezier {
            0%  {
                transform: translateY(0px);
            }
            10% {
                transform: translateY(-2px) scale(1.4,.6);
            }
            15% {
                transform: translateY(-2.7px) scale(1.44,.6);
            }
            25% {
                transform: translateY(0px) scale(.9,1.1);
            }
            45% {
                transform: translateY(45px) scale(1,1);
            }
            50% {
                transform: translateY(46px) scale(1.2,.8);
            }
            55% {
                transform: translateY(46px) scale(1.1,.9);
            }
            65% {
                transform: translateY(35px) scale(.9,1.1);
            }
            100% {
                transform: translateY(0px);
            }
        }
    </style>
    <script>
ymaps.ready(function () {
    var map = new ymaps.Map('map', {
        center: [53.86983521631579, 27.4866678052524],
        zoom: 17
    });
    // Создадим макет метки.
    var animatedLayout = ymaps.templateLayoutFactory.createClass(
        '<div class="placemark"></div>',
        {
            build: function () {
                animatedLayout.superclass.build.call(this);
                var element = this.getParentElement().getElementsByClassName('placemark')[0];
                // Если метка выбрана, то увеличим её размер.
                var size = this.isActive ? 60 : 34;
                // При задании для метки своего HTML макета, фигуру активной области
                // необходимо задать самостоятельно - иначе метка будет неинтерактивной.
                // Создадим фигуру активной области "Круг".
                var smallShape = {type: 'Circle', coordinates: [0, 50], radius: size / 0.9};
                var bigShape = {type: 'Circle', coordinates: [0, 0], radius: size / 0.9};
                // Зададим фигуру активной области.
                this.getData().options.set('shape', this.isActive ? bigShape : smallShape);
                // Если метка выбрана, то зададим класс и запустим анимацию.
                if (this.isActive) {
                    element.classList.add("active");
                    element.style.animation = ".35s show-big-placemark";
                    
                } else if (this.inited) {
                    element.classList.remove("active");
                    element.style.animation = ".35s show-small-placemark";
                }
                if (!this.inited) {
                    this.inited = true;
                    this.isActive = false;
                    // При клике по метке будем перестраивать макет.
                    this.getData().geoObject.events.add('click', function () {
                        this.isActive = !this.isActive;
                        this.rebuild();
                    }, this);
                }
            }
        }
    );
    map.geoObjects.add(new ymaps.Placemark([53.86983521631579, 27.4866678052524], {}, {
        iconLayout: animatedLayout,
        hasBalloon: false,
    }));
});        
    </script>
Минск
Телефоны:
- мобильный
+375 (29) 680-90-35
+375 (29) 129-95-14
- городской
+375 (17) 210-50-71
Mail:
limemedia@gothetour.BY
Skype:
Vikanazarenko
Адрес:
г.Минск пр-т Газеты Правда,9а
    <p>Анимированная метка:)</p>
    <div id="map"></div>
