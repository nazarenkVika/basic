<?php
   /* @var $this yii\web\View */
   use yii\widgets\LinkPager;
   use yii\helpers\Html;
   use yii\helpers\Url;
   
   $this->title = 'Tours';
   $this->params['breadcrumbs'][] = $this->title;
   ?>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
 
 .wrapper {
  display: table;
  height: 100%;
  width: 100%;
}

.container-fostrap {
  display: table-cell;
  padding: 1em;
  text-align: center;
  vertical-align: middle;
}
.fostrap-logo {
  width: 100px;
  margin-bottom:15px
}
h1.heading {
  color: #fff;
  font-size: 1.15em;
  font-weight: 900;
  margin: 0 0 0.5em;
  color: #505050;
}
@media (min-width: 450px) {
  h1.heading {
    font-size: 3.55em;
  }
}
@media (min-width: 760px) {
  h1.heading {
    font-size: 3.05em;
  }
}
@media (min-width: 900px) {
  h1.heading {
    font-size: 3.25em;
    margin: 0 0 0.3em;
  }
} 
.card {
  display: block; 
    margin-bottom: 20px;
    line-height: 1.42857143;
    background-color: #fff;
    border-radius: 2px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12); 
    transition: box-shadow .25s; 
    border-radius: 10px;
}
.card:hover {
  box-shadow: 0 8px 17px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
}
.img-card {
  width: 100%;
  height:200px;
  border-top-left-radius:2px;
  border-top-right-radius:2px;
  display:block;
    overflow: hidden;
    
}
.img-card img{
  width: 100%;
  height: 200px;
  object-fit:cover; 
  transition: all .25s ease;
  border-radius: 10px 10px 0px 0px;
  
} 
.card-content {
  padding:15px;
  text-align:left;
}
.card-title {
  margin-top:0px;
  font-weight: 700;
  font-size: 1.65em;
}
.card-title a {
  color: #000;
  text-decoration: none !important;
}
.card-read-more {
  border-top: 1px solid #D4D4D4;
}
.card-read-more a {
  text-decoration: none !important;
  padding:10px;
  font-weight:600;
  text-transform: uppercase
}
@import "lesshat";

// 0. Variables         ============

@timing:         0.08s;
@blue:           #305ae3;
@red:            #f94877; 

// 0.1 Body             ============

body{
  background: #e0e0e0;
}

html{
  -webkit-font-smoothing: antialiased;
}

*{
  box-sizing: border-box;
}

h1{
  font-family: 'Ubuntu', sans-serif;
  font-weight: 300;
  text-align: center;
  margin-top: 60px;
  margin-bottom: 15px;}
  
  span{
    font-size: 1.4em;
    color: @red;
    font-weight: 700;
    font-style: italic;
  }


p{
  text-align: center;
  color: #949494;
  margin-top: 0;
  margin-bottom: 0;
}


</style>
      <div class="container">
         <div class="row">
            <h1 class="block-title" >Лучшие туры </h1>
         </div>

         <section class="wrapper">
    <div class="container-fostrap">
        <div>
            <!-- <h1 class="heading">
                Все туры
            </h1> -->
        </div>
        
        <div class="content">
            <div class="container">
                <div class="row">
            <?php foreach($tours as $tour):?>

                    <div class="col-xs-12 col-sm-4">
                        <div class="card">
                          <a class="img-card" href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id]);?>"><img class="card-img-top" src="<?= $tour->getImage();?>" alt=""></a>
                            <div class="card-content">
                                <h4 class="card-title">
                                <a href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id]);?>"><?= $tour->title?></a>
                                </h4>
                                <p class="">
                                <?= $tour->description?>
                                </p>
<p>
    <a href="<?= Url::toRoute(['site/category', 'id'=>$tour->category->id]);?>"><?= $tour->category->title;?></a>
            </p>

                            </div>
                            <div class="card-read-more">
                                <a class="btn btn-link btn-block" href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id]);?>">Подробнее</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?> 

                </div>
            </div>
        </div>
    </div>
          <!--pagination-->        
          <?php 
         echo LinkPager::widget([
          'pagination' => $pagination,
          ]);
         ?>

</section>


   <!-- вызов сайдбара -->
   <?= $this->render('/partials/sidebar',[
      'popular'=>$popular,
      'recent'=>$recent,
      'categories'=>$categories,
      ]);?>
         </div> 