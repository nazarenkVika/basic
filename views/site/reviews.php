<?php
	/* @var $this yii\web\View */
	use yii\widgets\LinkPager;
	use yii\helpers\Html;
   use yii\helpers\Url;
use yii\widgets\ActiveForm;
	
	$this->title = 'Reviews';
	$this->params['breadcrumbs'][] = $this->title;
	?>


<div class="leave-comment">
   <?php if(Yii::$app->session->getFlash('reviews')):?>
      <div class="alert alert-succes" role="alert">
         <?= Yii::$app->session->getFlash('reviews');?>
      </div>
   <?php endif;?>
   </div>

   <?php if(!Yii::$app->user->isGuest):?>
         <div>
			<?php $form = \yii\widgets\ActiveForm::begin([
				'action'=>['site/reviews'],
				'options'=> ['class'=>'form-horizontal contact-form', 'role'=>'form']])?>
			<div class="form-group">
				<div class="col-md-12">
					<?= $form->field($reviewsForm,'review')->textarea(['class'=>'form-control','placeholder'=>'Write Message'])->label(false);?>
					<!-- <textarea class="form-control" rows="6" name="message" placeholder="Оставьте свой отзыв"></textarea> -->
				</div>
			</div>
			<button type="submit" href="#" class="btn send-btn">Опубликовать</a> 
			<?php \yii\widgets\ActiveForm::end();?>
         
         </div>
         <?php endif;?>
<div class="site-tours">
<link href="../Gothetour/uploads/bootstrap/bootstrap.min.css" rel="stylesheet">
<link rel='stylesheet' id='style.owl.carousel-css'  href='../Gothetour/plugins/owl-carousel/css/owl.carousele049.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='style.owl.carousel.theme-css'  href='../Gothetour/plugins/owl-carousel/css/owl.themee049.css?ver=4.7.8' type='text/css' media='all' />
<link rel='stylesheet' id='style.owl.carousel.styles-css'  href='../Gothetour/plugins/owl-carousel/css/stylese049.css?ver=4.7.8' type='text/css' media='all' />
<script type='text/javascript' src='../Gothetour/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
<script type='text/javascript' src='../Gothetour/plugins/owl-carousel/js/owl.carousele049.js?ver=4.7.8'></script>
<script type='text/javascript' src='../Gothetour/plugins/owl-carousel/js/scripte049.js?ver=4.7.8'></script>
<link rel="stylesheet" href="../Gothetour/themes/style68b3.css?ver=1" type="text/css">
<link rel="stylesheet" href="../Gothetour/css/color-shemes/yellow-orange/yellow-orange.css" type="text/css">
<div id="content" class="reviews">
<div class="wrap">
<div class="с-12">
<div class="page page-content">
<div class="reviews-content">
	<div id="blog-background">
		<div id="content">
			<h1 class="block-title" style="margin-top: -8px;">Отзывы клиентов</h1>
		</div>
	</div>
	<div
		class="carousel-reviews">
		<div id="owl-carousel-677717799" class="owl-carousel owl-carousel-reviews-carousel"  data-singleitem="true"  data-autoplay="false"  data-navigation="true"  data-pagination="false" >
			<?php if(!empty($reviews)):?>
			<?php foreach($reviews as $review):?>
			<div class="item">
				<div class="owl-carousel-item-text">
					<img style="width:150px;" src="<?= $review->user->image;?>"> 
					<h5><?= $review->user->name?></h5>
					<blockquote><?=$review->title;?></blockquote>
					<h3><?= $review->getDate();?></h3>
					<h2><?=$review->text;?></h2>
				</div>
			</div>
			<?php endforeach;?>
			<?php endif;?>

		</div>
	</div>
</div>