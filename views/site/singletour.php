<?php
   use yii\helpers\Url;
   use yii\helpers\Html;
   use app\models\Reservation;
   use yii\bootstrap\Modal;
   use yii\widgets\Pjax;
   ?>
<!--main content start-->
<script src="http://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="../../web/Gothetour/js/counter_reservation.js"></script>
<link rel="stylesheet" type="text/css" href="../../web/Gothetour/css/count_reservation.css"/>
<div class="main-content">
   <div class="container">
      <div class="row">
         <div class="col-md-8">
            <article class="post">
               <div class="post-thumb">
                  <a href="blog.html"><img src="<?= $tour->getImage();?>" alt=""></a>
               </div>
               <div class="post-content">
                  <header class="entry-header text-center text-uppercase">
                     <h6><a href="<?= Url::toRoute(['site/category','id'=>$tour->category->id])?>"> <?= $tour->category->title?></a></h6>
                     <h1 class="entry-title"><a href="#"><?= $tour->title?></a></h1>
                  </header>
                  <?php foreach((array)$tour->getGallery() as $image):?>
                  <img src="<?='http://localhost/Gothetour/basic/web/uploads/'.$image;?>" alt="">
                  <?php endforeach; ?> 
                  <div class="entry-content">
                     <?= $tour->content?>
                  </div>
                  <div class="decoration">
                     <a href="#" class="btn btn-default">Decoration</a>
                     <a href="#" class="btn btn-default">Decoration</a>
                  </div>


                  <?php if(!Yii::$app->user->isGuest):?>
                     <?php if(is_null($reservation)):?>
                        <?php if($tour->getAvailablePlace() == 0):?>
                           <a href="#" class="btn btn-default">net mest</a>
                        <?php else:?>
                        <?php Modal::begin([
                           'header' => '<h2>Бронирование</h2>',
                           'toggleButton' => ['label' => 'Забронировать'],]); ?>
                        <?php $form = \yii\widgets\ActiveForm::begin(['action'=>['site/reserve', 'id'=>$tour->id]])?>
                           <!-- Picture and name of restaurant -->
                           <section class="restaurant">
                              <h1>ВСТАВИТЬ ССЫЛКУ И НАЗВАНИЕ ТУРА</h1>
                           </section>
                           <!-- Number of tables to book -->
                           <section class="number-of-tables">
                           <h2>КОЛИЧЕСТВО МЕСТ</h2>
                           <div class="number">
                            <span class="minus">-</span>
                              <?= Html::activeTextInput($reserevationForm, 'count',[
                              'id' => 'counter',
                              'type'=> 'number',
                              'max' => $tour->getAvailablePlace(),
                              'value' => 1, 
                              'readonly'=>'readonly',
                              'type' => 'number',]) ?>
                              <span class="plus">+</span>
                           </div>
                           </section>
                           <!-- Date and time -->
                           <section class="choose-date">
                           <h2>ДАТА <span>И</span> ВРЕМЯ</h2>
                           <div>05 May 2017 <span>-</span> 19:30</div>
                           </section>
                           <button type="submit" class="confirm-book">Забронировать</button>
                        <?php \yii\widgets\ActiveForm::end();?>
                        <?php Modal::end(); ?> 
                        
                        <?php endif;?>
                  <?php else:
                     switch ($reservation->status):
                        case Reservation::STATUS_FINISHED:?>
                  <a href="#" class="btn btn-default">STATUS_FINISHED</a>
                  <?php  break;?>
                  <?php case Reservation::STATUS_ACCEPTED:?>
                  <a href="#" class="btn btn-default">STATUS_ACCEPTED</a>
                  <?php  break;?>
                  <?php case Reservation::STATUS_PENDING:?>
                  <a href="#" class="btn btn-default">STATUS_PENDING</a>
                  <?php  break;
                     default:
                     endswitch;
                     endif;
                     endif;?>


                  <div class="social-share">
                     <span
                        class="social-share-title pull-left text-capitalize">By On <?= $tour->getDate();?></span>
                     <ul class="text-center pull-right">
                        <li><a class="s-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="s-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="s-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a class="s-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a class="s-instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                     </ul>
                  </div>
               </div>
            </article>
            <?= $this->render('/partials/comment', [ 
               'tour'=>$tour,
               'comments'=>$comments,
               'commentForm'=>$commentForm
               ])?>
            <?= $this->render('/partials/sidebar', [
               'popular'=>$popular,
               'recent'=>$recent,
               'categories'=>$categories
               ]);?>
         </div>
      </div>
   </div>
</div>