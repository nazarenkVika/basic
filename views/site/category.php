<?php 
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<!--main content start-->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php foreach($tours as $tour):?>
                <article class="post post-list">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="post-thumb">
                                <a href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id])?>"><img src="<?= $tour->getImage();?>" alt="" class="pull-left"></a>

                                <a href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id])?>" class="post-thumb-overlay text-center">
                                    <div class="text-uppercase text-center">View Post</div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="post-content">
                                <header class="entry-header text-uppercase">
                                    <h6><a href="<?= Url::toRoute(['site/category', 'id'=>$tour->category->id]);?>"> <?= $tour->category->title?></a></h6>

                                    <h1 class="entry-title"><a href="<?= Url::toRoute(['site/singletour', 'id'=>$tour->id])?>"><?= $tour->title?></a></h1>
                                </header>
                                <div class="entry-content">
                                    <p><?= $tour->description?>
                                    </p>
                                </div>
                                <div class="social-share">
                                    <span class="social-share-title pull-left text-capitalize">By Rubel On <?= $tour->getDate();?></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <?php endforeach;?>
                <ul class="pagination">
                <!--pagination-->        
                <?php 
                  echo LinkPager::widget([
                   'pagination' => $pagination,
                   ]);
                  ?>
                </ul>
            </div>
            <?= $this->render('/partials/sidebar',[
                'popular'=>$popular,
                'recent'=>$recent,
                'categories'=>$categories,
            ]);?>
        </div>
    </div>
</div>
<!-- end main content-->
<!--footer start-->
<div id="footer">
    <div class="footer-instagram-section">
        <h3 class="footer-instagram-title text-center text-uppercase">Instagram</h3>

        <div id="footer-instagram" class="owl-carousel">

            <div class="item">
                <a href="#"><img src="assets/images/ins-1.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="assets/images/ins-2.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="assets/images/ins-3.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="assets/images/ins-4.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="assets/images/ins-5.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="assets/images/ins-6.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="assets/images/ins-7.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="assets/images/ins-8.jpg" alt=""></a>
            </div>

        </div>
    </div>
</div>