<?php

/* @var $this yii\web\View */

$this->title = 'Турагентство GOTHETOUR';
?>
<div class="site-index">
<script type='text/javascript'src='../Gothetour/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
<script type='text/javascript'src='../Gothetour/js/slick68b3.js?ver=1'></script>
<script type='text/javascript'src='../Gothetour/js/animation/ScrollMagic68b3.js?ver=1'></script>
<script type='text/javascript'src='../Gothetour/js/animation/TweenMax68b3.js?ver=1'></script>
<script type='text/javascript'src='../Gothetour/js/animation/animation.gsap68b3.js?ver=1'></script>
<script type='text/javascript'src='../Gothetour/plugins/easy-fancybox/js/jquery.mousewheel.min4830.js?ver=3.1.12"'></script>
<script type='text/javascript'src='../Gothetour/js/animation/mousewheel_action.js'></script>
<script type='text/javascript'src='../Gothetour/js/animation-home.js'></script>
<script type='text/javascript'src='../Gothetour/js/script_main.js'></script> 

		<!--отображение -->
		<!-- <script type='text/javascript' src='wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
		<script type='text/javascript' src='wp-content/themes/slick/slick68b3.js?ver=1'></script> -->
		<!--скрол колесом -->
		<!-- <script type='text/javascript' src='wp-content/themes/js/animation/ScrollMagic68b3.js?ver=1'></script>
		<script type='text/javascript' src='wp-content/themes/js/animation/animation.gsap68b3.js?ver=1'></script> -->
		<!-- Отвечают за скрол ручками -->
		<!-- <script src="wp-content/themes/js/animation/mousewheel_action.js"></script>
		<script type='text/javascript' src='wp-content/plugins/easy-fancybox/js/jquery.mousewheel.min4830.js?ver=3.1.12'></script> -->
		<!--Про кнопке поехали автоматически-->
		<!-- <script type='text/javascript' src='wp-content/themes/js/animation/TweenMax68b3.js?ver=1'></script> -->

</head>
<body class="home blog">
		<link rel="stylesheet" href="../Gothetour/css/style68b3.css?ver=1" type="text/css">
		
		<div id="loadImages" style="display:none;">
		</div>


<!-- Задник - машинки-->
		<div id="main-wrapper" class="horizontal">
			<div class="cloud size lg one"></div>
			<div class="cloud size md one"></div>
			<div class="cloud size sm one"></div>
			<div class="cloud size xs one"></div>
			<div class="cloud size lg two"></div>
			<div class="cloud size md two"></div>
			<div class="cloud size sm two"></div>
			<div class="cloud size xs two"></div>
			<section class="element">
				<div class="horizon background">
					<img src="../Gothetour/images/backgrounds/background-horizon.svg"/>
				</div>

<!--Указатель Жми сюда-->
				<div class="front background">

					<svg id="testScrollTo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 280 120" onclick="ga('send', 'event', 'Here', 'Click');">
						<g xmlns:xlink="/moscow/">
							<path d="M3.37988,65.59l34.54-35.09a8.2397,8.2397,0,0,1,5.85986-2.47l226.14014-1.21a8.32006,8.32006,0,0,1,8.2998,8.25l0.37012,69.8a8.3001,8.3001,0,0,1-8.25,8.34l-226.1001,1.21h0a8.24046,8.24046,0,0,1-5.83984-2.41l-35-34.71A8.31,8.31,0,0,1,3.37988,65.59Z" transform="translate(0.53788)" fill="white" stroke="white" stroke-width="2px"/>
							<path d="M7.06982,69.23L41.6499,34.15a3.12031,3.12031,0,0,1,2.2002-.93l226.06982-1.21h0a3.11,3.11,0,0,1,3.10986,3.09l0.37012,69.79a3.10978,3.10978,0,0,1-3.08984,3.13l-226.06982,1.2h0a3.11,3.11,0,0,1-2.18994-.9l-35-34.71A3.10972,3.10972,0,0,1,7.06982,69.23Z" transform="translate(0.53788)" fill="#4b5d66"/>
							<polygon points="44.408 36.33 270.478 35.12 270.858 104.91 44.778 106.12 9.828 71.41 44.408 36.33" fill="#d24a43"/>
							<polygon points="270.478 35.12 268.028 37.41 267.478 101.38 42.848 104.2 44.778 106.12 270.838 105.36 270.478 35.12" fill="#b9433b"/>
							<g opacity="0.2" fill="white">
								<path d="M187.02,38.33l29.5-.16c4.83984,2.81,28.8999,17.28,34.56006,30.5,6.2998,14.72,14.68994,32,14.68994,32l-19.46.1s-12.62988-17.91-23.18994-24.52C213.31006,70.17,190.2998,42.34,187.02,38.33Z" transform="translate(0.53788)"/>
								<polygon points="169.958 38.42 220.328 100.96 164.478 101.26 135.228 38.61 169.958 38.42"/>
								<polygon points="157.748 101.29 127.798 101.45 118.068 38.7 129.688 38.64 157.748 101.29"/>
								<polygon points="59.858 39.01 85.268 38.88 88.018 101.67 48.608 101.88 59.858 39.01"/>
							</g>
							<polygon points="270.478 35.12 268.028 37.41 45.458 38.36 9.828 71.41 44.408 36.33 270.478 35.12" opacity="0.2" fill="white" style="isolation:isolate"/>
						</g>
						<text x="-215" y="-63" class="testScrollTo-text">Жми сюда</text>

					</svg>

<!--Background2-->
					<img src="../Gothetour/images/backgrounds/background-frontw.svg"/>
				</div>
									<!-- search -->

				<div id="woman" class="item woman">
					<img src="../Gothetour/images/backgrounds/background-second1.svg"/>
				</div>
				<div id="car" class="item car">
					<img src="../Gothetour/images/backgrounds/background-second4.svg"/>
				</div>
				<div id="bus" class="item bus">
					<img src="../Gothetour/images/backgrounds/background-second2.svg"/>
				</div>
				<div id="truck" class="item truck">
					<img src="../Gothetour/images/backgrounds/background-second3.svg"/>
				</div>
				<div id="smoke">
					<img id="smoke1" src="../Gothetour/images/smoke3.svg" alt="smoke1">
					<img id="smoke2" src="../Gothetour/images/smoke2.svg" alt="smoke2">
					<img id="smoke3" src="../Gothetour/images/smoke1.svg" alt="smoke3">
				</div>
				<div class="spacer s2"></div>
			</section>
			<!--Люди-->
			<style>
				.bron{
				width: 60% !important;
				height: 100% !important;
				margin-left:1440px;
				margin-bottom: 45px;
				}
				.comment{
				width: 55% !important;
				height: 100% !important;
				margin-left:1180px;
				margin-bottom: 55px;
				}
				.maps{
				width: 35% !important;
				height: 100% !important;
				margin-left:70px;
				margin-bottom: 30px;
				}
				#form_search{
					margin-top:-45px;
					margin-left:1000px;
				}

			</style>


			<div id="boy">
				










      <link rel="stylesheet" href="../Quiz/style.css">
      <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700'>
      <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
      <link rel="stylesheet" href="../Quiz/style1.css">
      <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>


      <!-- partial:index.partial.html -->
      <div class="btn_container">
         <a class="open_button"style="margin-left: 1572px; margin-bottom: -160px;" href="#">Подобрать тур</a>
      </div>
      
      <div class="modal_info" style="">
         <!-- partial:index.partial.html -->
         <!-- <div id="needsjs" class="needsjs">
            <p>This quiz relies on javascript to work - please turn javascript on in your browser setting to complete the quiz.</p>
         </div> -->
         <div id='quiz'>
            <div class="startpanel panel transition current">
               <div class="image" style="background:url('../Quiz/img/1.jpg') 50% 50%; background-size:cover;">
               </div>
               <div class="start" style="z-index:502;">
                  <h3>Подберём лучший вариант тура под ваши критерии</h3>
                  <a class="nextbutton">Подобрать тур</a>
               </div>
            </div>
            <div class="panel transition">
               <div class="image" style="background:url('../Quiz/img/2.jpg') 50% 50%; background-size:cover;">
                  <a class="subtlelink quicktransition retake">Рестарт</a>
               </div>
               <div class="question">
                  <h3>Выберите бюджет тура, который Вы считаете привлекательным</h3>
                  <input type="radio" name="price" id="priceoption1" value="2">
                  <label for="priceoption1" class="">
                  до 50 рублей
                  </label>
                  <input type="radio" name="price" id="priceoption2" value="4">
                  <label for="priceoption2" class="">
                  от 50 до 100 рублей
                  </label>
                  <input type="radio" name="price" id="priceoption3" value="6">
                  <label for="priceoption3" class="">
                  от 100 до 200 рублей
                  </label>
                  <input type="radio" name="price" id="priceoption4" value="8">
                  <label for="priceoption4" class="">
                  от 200 до 400 рублей
                  </label>
                  <input type="radio" name="price" id="priceoption5" value="10">
                  <label for="priceoption5" class="">
                  от 400 рублей
                  </label>
                  <a class="nextbutton">Вперед</a>
               </div>
            </div>
            <div class="panel transition">
               <div class="image" style="background:url('../Quiz/img/3.jpg') 50% 50%; background-size:cover;" data-copyright="Tomás Fano">
                  <a class="subtlelink quicktransition retake">Рестарт</a>
               </div>
               <div class="question">
                  <h3>Выберите, что для вас предпочтительнее </h3>
                  <input type="radio" name="fun" id="funoption1" value="1">
                  <label for="funoption1" class="">
                  Цена тура
                  </label>
                  <input type="radio" name="fun" id="funoption2" value="2">
                  <label for="funoption2" class="">
                  Спокойный отдых
                  </label>
                  <input type="radio" name="fun" id="funoption3" value="3">
                  <label for="funoption3" class="">
                  Крутые фотки в Instagram
                  </label>
                  <input type="radio" name="fun" id="funoption4" value="4">
                  <label for="funoption4" class="">
                  Много развлечений
                  </label>
                  <input type="radio" name="fun" id="funoption5" value="5">
                  <label for="funoption5" class="">
                  Экстрим
                  </label>
                  <a class="lastbutton">Назад</a>
                  <a class="nextbutton">Вперед</a>
               </div>
            </div>
            <div class="panel transition">
               <div class="image" style="background:url('../Quiz/img/4.jpg') 50% 50%; background-size:cover;">
                  <a class="subtlelink quicktransition retake">Рестарт</a>
               </div>
               <div class="question">
                  <h3>Какое количество стран/городов Вы хотели бы посетить?</h3>
                  <input type="radio" name="countPlace" id="countPlaceoption1" value="1">
                  <label for="countPlaceoption1" class="">
                  Одну
                  </label>
                  <input type="radio" name="countPlace" id="countPlaceoption2" value="2">
                  <label for="countPlaceoption2" class="">
                  Две(два)
                  </label>
                  <input type="radio" name="countPlace" id="countPlaceoption3" value="3">
                  <label for="countPlaceoption3" class="">
                  Три
                  </label>
                  <input type="radio" name="countPlace" id="countPlaceoption4" value="4">
                  <label for="countPlaceoption4" class="">
                  Четыре
                  </label>
                  <input type="radio" name="countPlace" id="countPlaceoption5" value="5">
                  <label for="countPlaceoption5" class="">
                  Пять
                  </label>
                  <a class="lastbutton">Назад</a>
                  <a class="nextbutton">Вперед</a>
               </div>
            </div>
            <div class="panel transition">
               <div class="image" style="background:url('../Quiz/img/5.jpg') 50% 50%; background-size:cover;">
                  <a class="subtlelink quicktransition retake">Рестарт</a>
               </div>
               <div class="question">
                  <h3>Как вы считате, какая длительность тура самая оптимальная?</h3>
                  <input type="radio" name="duration" id="durationoption1" value="5">
                  <label for="durationoption1" class="">
                  1 день
                  </label>
                  <input type="radio" name="duration" id="durationoption2" value="6">
                  <label for="durationoption2" class="">
                  2 дня
                  </label>
                  <input type="radio" name="duration" id="durationoption3" value="7">
                  <label for="durationoption3" class="">
                  3 дня
                  </label>
                  <input type="radio" name="duration" id="durationoption4" value="8">
                  <label for="durationoption4" class="">
                  5 дней
                  </label>
                  <input type="radio" name="duration" id="durationoption5" value="10">
                  <label for="durationoption5" class="">
                  10 дней
                  </label>
                  <a class="lastbutton">Назад</a>
                  <a class="nextbutton">Вперед</a>
               </div>
            </div>
            <div class="panel transition">
               <div class="image" style="background:url('../Quiz/img/6.jpg') 50% 50%; background-size:cover;">
                  <a class="subtlelink quicktransition retake">Рестарт</a>
               </div>
               <div class="question">
                  <h3>Выберите характеристику для тура</h3>
                  <input type="radio" name="rating" id="ratingoption1" value="1">
                  <label for="ratingoption1" class="">
                  Бюджетный
                  </label>
                  <input type="radio" name="rating" id="ratingoption2" value="2">
                  <label for="ratingoption2" class="">
                  Новый
                  </label>
                  <input type="radio" name="rating" id="ratingoption3" value="3">
                  <label for="ratingoption3" class="">
                  Стандартный
                  </label>
                  <input type="radio" name="rating" id="ratingoption4" value="4">
                  <label for="ratingoption4" class="">
                  Популярный
                  </label>
                  <input type="radio" name="rating" id="ratingoption5" value="5">
                  <label for="ratingoption5" class="">
                  В топе рейтинга
                  </label>
                  <a class="lastbutton">Назад</a>
                  <a class="nextbutton" id="calc">Узнать результат</a>
               </div>
            </div>
            <div class="panel resultspanel transition">
               <div class="image" id="finalimage" style="background:url('http://placekitten.com/300/400') 50% 50%; background-size:cover;"></div>
               <div class="question" style="overflow-y: scroll;">
                  <h3>Вам подходит <span id="result"></span></h3>
                  <p id="desc">
                     123
                  </p>
                  <a class="buttonlink transition retake">Пройти заново</a>
               </div>
            </div>
         </div>
      </div>
      <div class="modal_overlay"></div>
      <!--partial -->
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <!-- partial -->
      <script  src="../Quiz/script.js"></script>
      <script src="../Quiz/script1.js"></script>









			
				<a href="reserved"style="margin-top:5000px;"><img class="bron" src="../Gothetour/images/backgrounds/bron.png"/></a>
				<a href="reviews"><img class="comment" src="../Gothetour/images/backgrounds/comment.png"/></a>
				<a href="contact"><img class="maps" src="../Gothetour/images/backgrounds/mesto.png"/></a>
				<br>
				<img id="boyImg" src="../Gothetour/images/boy/people1.svg" alt="boy">
			</div>
	
		</div>

<!--Указатель-->
		<script>
			function redirectToAsync(url, delay){
			    setTimeout(function(){
			        document.location.href = url;
			    }, delay);
			}
		</script>
		<div id="column">
			<svg id="boards"  xmlns="http://www.w3.org/2000/svg" data-name="Boards" viewBox="0 0 280.15 537.8" style=" margin-left:3100px;">
				<path fill="#4b5d66" d="M146.26 34.82h21.17v395.82h-21.17z"/>
				<path fill="#5d7b7e" d="M146.26 34.82h8.49v395.82h-8.49z"/>
				<path fill="#4b5d66" d="M182.19 530.9H131.5l9.81-96.5h31.07l9.81 96.5zM173.35 402.77a6.9 6.9 0 0 1-6.9 6.89h-19.21a6.89 6.89 0 0 1-6.9-6.89 6.89 6.89 0 0 1 6.9-6.89h19.22a6.9 6.9 0 0 1 6.89 6.89z"/>
				<path fill="#5d7b7e" d="M149.33 397.99l14-2.09h-16.1a6.89 6.89 0 0 0-3.7 12.69 6.89 6.89 0 0 1 5.8-10.6z"/>
				<path fill="#4b5d66" d="M167.97 23.18a4.65 4.65 0 0 1-4.65 4.64h-13a4.65 4.65 0 0 1-4.65-4.64 4.65 4.65 0 0 1 4.65-4.65h13a4.65 4.65 0 0 1 4.65 4.65z"/>
				<circle cx="157.87" cy="10.41" r="10.41" fill="#4b5d66"/>
				<ellipse cx="157.87" cy="5.07" fill="#fff" opacity=".4" rx="5.7" ry="3.26"/>
				<path fill="#fff" d="M152.46 20.62s12.68-2.09 10.86-2.09h-13a4.63 4.63 0 0 0-2.06 8.79 4.58 4.58 0 0 1-.5-2.05 4.65 4.65 0 0 1 4.65-4.65z" opacity=".4"/>
				<path fill="#5d7b7e" d="M150.37 530.9H131.5l9.81-96.5h10.2l-1.14 96.5z"/>
				<path fill="#4b5d66" d="M184.99 524.02h-56.3c-5 0-9.1 3.08-9.1 6.89s4.08 6.89 9.1 6.89H185c5 0 9.1-3.09 9.1-6.89s-4.07-6.89-9.1-6.89zM178.64 430.65c0 3.81-4.08 6.89-9.11 6.89h-25.37c-5 0-9.11-3.09-9.11-6.89 0-3.81 4.08-6.89 9.1-6.89h25.39c5 0 9.1 3.09 9.1 6.89z"/>
<!-- СИНЯЯ -->
				<a href="javascript:redirectToAsync('blog/Рим/index.html', 3000)">
					<g fill="#fff" xmlns:xlink="/moscow/" onclick="firstBoardClicked()" id="first-boar">
						<path stroke="#fff" stroke-width="2" d="M3.92 65.59L38.46 30.5a8.24 8.24 0 0 1 5.86-2.47l226.14-1.21a8.32 8.32 0 0 1 8.3 8.25l.37 69.8a8.3 8.3 0 0 1-8.25 8.34l-226.1 1.21a8.24 8.24 0 0 1-5.84-2.41l-35-34.71a8.31 8.31 0 0 1-.02-11.71z"/>
						<path fill="#4b5d66" d="M7.6 69.23L42.2 34.15a3.12 3.12 0 0 1 2.2-.93l226.07-1.21a3.11 3.11 0 0 1 3.1 3.09l.38 69.79a3.11 3.11 0 0 1-3.1 3.13l-226.06 1.2a3.11 3.11 0 0 1-2.2-.9l-35-34.71a3.11 3.11 0 0 1 .03-4.38z"/>
						<path fill="#5ba3c1" d="M44.41 36.33l226.07-1.21.38 69.79-226.08 1.21L9.83 71.41l34.58-35.08z"/>
						<path fill="#5ba3c1" d="M270.48 35.12l-2.45 2.29-.55 63.97-224.63 2.82 1.93 1.92 226.06-.76-.36-70.24z"/>
						<g opacity=".2">
							<path d="M187.56 38.33l29.5-.16c4.84 2.81 28.9 17.28 34.56 30.5 6.3 14.72 14.69 32 14.69 32l-19.46.1s-12.63-17.91-23.2-24.52c-9.8-6.08-32.81-33.91-36.1-37.92zM169.96 38.42l50.37 62.54-55.85.3-29.25-62.65 34.73-.19zM157.75 101.29l-29.95.16-9.73-62.75 11.62-.06 28.06 62.65zM59.86 39.01l25.41-.13 2.75 62.79-39.41.21 11.25-62.87z"/>
						</g>
						<path d="M270.48 35.12l-2.45 2.29-222.57.95L9.83 71.41l34.58-35.08 226.07-1.21z" opacity=".2"/>
					</g>
				</a>
				<g xmlns:xlink="/moscow/" fill="#fff" onclick="firstBoardClicked()" id="register-child">
					<a href="javascript:redirectToAsync('blog/Рим/index.html', 3000)">
						<defs>
							<style>.cls-1{fill:#fff;}</style>
						</defs>
						<title>rome</title>
						<path class="cls-1" d="M23.34,8h5.17a8.09,8.09,0,0,1,5.94,2.46,8.14,8.14,0,0,1,2.47,5.94,8.1,8.1,0,0,1-2.47,5.94,8.09,8.09,0,0,1-5.94,2.46H24.64v7.76a1.3,1.3,0,0,1-1.3,1.29,1.22,1.22,0,0,1-.91-.38,1.24,1.24,0,0,1-.38-.91V9.24A1.3,1.3,0,0,1,23.34,8Zm1.3,2.59V22.17h3.87a5.82,5.82,0,0,0,5.82-5.82,5.8,5.8,0,0,0-5.82-5.81Z" transform="translate(100 50)"/>
						<path class="cls-1" d="M56.3,32.51A1.3,1.3,0,0,1,55,33.8a1.22,1.22,0,0,1-.91-.38,1.24,1.24,0,0,1-.38-.91v-1A8.4,8.4,0,0,1,39.5,25.4V17a1.24,1.24,0,0,1,.38-.91,1.22,1.22,0,0,1,.91-.38A1.3,1.3,0,0,1,42.09,17v8.4A5.81,5.81,0,0,0,52,29.51a5.57,5.57,0,0,0,1.71-4.11V17a1.24,1.24,0,0,1,.38-.91,1.22,1.22,0,0,1,.91-.38A1.3,1.3,0,0,1,56.3,17Z" transform="translate(100 50)"/>
						<path class="cls-1" d="M69.26,32.83,65.35,21.1l-4,12a1.47,1.47,0,0,1-.26.37,1.25,1.25,0,0,1-.92.37,1.22,1.22,0,0,1-.91-.38A1.29,1.29,0,0,1,59,32l5.15-15.46a1.53,1.53,0,0,1,.31-.47,1.25,1.25,0,0,1,.9-.36,1.28,1.28,0,0,1,.92.36,1.7,1.7,0,0,1,.3.47l4,11.88,4-11.88a1.53,1.53,0,0,1,.31-.47,1.25,1.25,0,0,1,.9-.36,1.32,1.32,0,0,1,.93.36,1.66,1.66,0,0,1,.29.47L82.06,32A1.29,1.29,0,0,1,80,33.43a1.29,1.29,0,0,1-.27-.37l-4-12L71.78,32.83a1.36,1.36,0,0,1-.33.6,1.29,1.29,0,0,1-.93.37,1.22,1.22,0,0,1-.9-.37A1.66,1.66,0,0,1,69.26,32.83Z" transform="translate(100 50)"/>
					</a>
				</g>
<!-- Коричневая -->
				<a href="javascript:redirectToAsync('blog/Париж/index.html', 3000)">
					<g fill="#fff"  onclick="fourthBoardClicked()" id="fourth-board">
						<path stroke="#fff" stroke-width="2" d="M3.92 347.07l34.54-35.09a8.24 8.24 0 0 1 5.86-2.47l226.14-1.2a8.32 8.32 0 0 1 8.3 8.25l.37 69.8a8.3 8.3 0 0 1-8.25 8.34l-226.1 1.21a8.25 8.25 0 0 1-5.84-2.41l-35-34.71a8.31 8.31 0 0 1-.02-11.72z"/>
						<path fill="#4b5d66" d="M7.6 350.71l34.59-35.08a3.12 3.12 0 0 1 2.2-.93l226.07-1.21a3.11 3.11 0 0 1 3.1 3.09l.38 69.79a3.11 3.11 0 0 1-3.1 3.13l-226.06 1.21a3.1 3.1 0 0 1-2.2-.9l-35-34.71a3.11 3.11 0 0 1 .03-4.39z"/>
						<path fill="#d38912" d="M44.41 317.81l226.07-1.21.38 69.79-226.08 1.21-34.95-34.71 34.58-35.08z"/>
						<path fill="#d37505" d="M270.48 316.6l-2.45 2.3-.55 63.96-224.63 2.82 1.93 1.92 226.06-.76-.36-70.24z"/>
						<g opacity=".2">
							<path d="M187.56 319.77l29.5-.16c4.84 2.81 28.9 17.27 34.56 30.5 6.3 14.72 14.69 32 14.69 32l-19.46.1s-12.63-17.91-23.2-24.52c-9.8-6.04-32.81-33.87-36.1-37.92zM169.96 319.9l50.37 62.54-55.85.3-29.25-62.65 34.73-.19zM157.75 382.77l-29.95.17-9.73-62.76 11.62-.06 28.06 62.65zM59.86 320.49l25.41-.13 2.75 62.79-39.41.21 11.25-62.87z"/>
						</g>
						<path d="M270.48 316.6l-2.45 2.3-222.57.94-35.63 33.05 34.58-35.08 226.07-1.21z" opacity=".2"/>
					</g>
					<g fill="#4b5d66" onclick="fourthBoardClicked()" id="trial-month">
				<a href="javascript:redirectToAsync('blog/Нью-Йорк/index.html', 3000)">
				<defs><style>.cls-1{fill:#fff;}</style></defs>
				<path class="cls-1" d="M8,14.43a1.09,1.09,0,0,1,.33-.8,1.11,1.11,0,0,1,.8-.33,1.13,1.13,0,0,1,1.13,1.13v9h11.3v-9A1.13,1.13,0,0,1,22.7,13.3a1.11,1.11,0,0,1,.8.33,1.09,1.09,0,0,1,.33.8V34.76a1.13,1.13,0,0,1-1.13,1.13,1.09,1.09,0,0,1-.8-.33,1.11,1.11,0,0,1-.33-.8v-9H10.27v9a1.11,1.11,0,0,1-.33.8,1.09,1.09,0,0,1-.8.33A1.13,1.13,0,0,1,8,34.76Z" transform="translate(80 328)"/>
				<path class="cls-1" d="M28.35,21.2v5.65h5.08a4.53,4.53,0,0,1,3.2,7.72,4.36,4.36,0,0,1-3.2,1.32H27.22a1.09,1.09,0,0,1-.8-.33,1.11,1.11,0,0,1-.33-.8V21.17a1.08,1.08,0,0,1,.34-.77,1.06,1.06,0,0,1,.79-.33,1.07,1.07,0,0,1,.79.33A1.12,1.12,0,0,1,28.35,21.2Zm5.08,12.43a2.29,2.29,0,0,0,2.26-2.26,2.27,2.27,0,0,0-2.26-2.26H28.35v4.52Z" transform="translate(80 328)"/>
				<path class="cls-1" d="M44.8,26.85A7.55,7.55,0,0,1,47,22.39,7.91,7.91,0,0,1,60.54,28,7.91,7.91,0,0,1,47,33.57a7.55,7.55,0,0,1-2.24-4.46H42.47v5.65a1.12,1.12,0,0,1-.34.8,1.07,1.07,0,0,1-.79.33,1.09,1.09,0,0,1-.8-.33,1.11,1.11,0,0,1-.33-.8V21.2a1.11,1.11,0,0,1,.33-.8,1.09,1.09,0,0,1,.8-.33,1.07,1.07,0,0,1,.79.33,1.12,1.12,0,0,1,.34.8v5.65Zm7.83-4.52A5.65,5.65,0,0,0,47,28a5.66,5.66,0,1,0,5.65-5.65Z" transform="translate(80 328)"/>
				<path class="cls-1" d="M63.93,29.11a1.13,1.13,0,1,1,0-2.26h6.78a1.07,1.07,0,0,1,.79.33,1.11,1.11,0,0,1,0,1.6,1.07,1.07,0,0,1-.79.33Z" transform="translate(80 328)"/>
				<path class="cls-1" d="M89.91,17.73,76.07,35.52a1.11,1.11,0,0,1-.85.37,1.14,1.14,0,0,1-1.13-1.13V14.43a1.14,1.14,0,0,1,1.13-1.13,1.13,1.13,0,0,1,1.13,1.13v17L90.16,13.71A1.13,1.13,0,0,1,91,13.3a1.11,1.11,0,0,1,.8.33,1.09,1.09,0,0,1,.33.8V34.76A1.13,1.13,0,0,1,91,35.89a1.09,1.09,0,0,1-.8-.33,1.11,1.11,0,0,1-.33-.8ZM78.7,8.21a1.11,1.11,0,0,1,.33-.8,1.13,1.13,0,0,1,1.6,0,1.11,1.11,0,0,1,.33.8c0,.31.22.58.66.8a3.54,3.54,0,0,0,1.6.33A3.6,3.6,0,0,0,84.82,9c.44-.22.67-.49.67-.8a1.11,1.11,0,0,1,.33-.8,1.13,1.13,0,0,1,1.6,0,1.11,1.11,0,0,1,.33.8,3,3,0,0,1-1.33,2.39,5.13,5.13,0,0,1-3.2,1,5.19,5.19,0,0,1-3.2-1A3,3,0,0,1,78.7,8.21Z" transform="translate(80 328)"/>
				<path class="cls-1" d="M110.24,28a7.91,7.91,0,1,1-13.5-5.59A7.91,7.91,0,0,1,110.24,28Zm-7.9-5.65A5.63,5.63,0,0,0,96.69,28a5.65,5.65,0,1,0,9.64-4A5.44,5.44,0,0,0,102.34,22.33Z" transform="translate(80 328)"/>
				<path class="cls-1" d="M114.76,41.54a1.13,1.13,0,0,1-2.26,0V28A7.91,7.91,0,1,1,126,33.57a7.89,7.89,0,0,1-11.18,0l-.06-.06Zm0-13.56a5.65,5.65,0,1,0,9.64-4,5.65,5.65,0,0,0-9.64,4Z" transform="translate(80 328)"/>
				<path class="cls-1" d="M130.57,21.2a1.12,1.12,0,0,1,.34-.8,1.07,1.07,0,0,1,.79-.33,1.09,1.09,0,0,1,.8.33,1.11,1.11,0,0,1,.33.8v6.17l8.29-7a1.13,1.13,0,0,1,1.55,0,1.11,1.11,0,0,1,.33.8,1.09,1.09,0,0,1-.32.79l-7,6,7,6a1.17,1.17,0,0,1,.32.81,1.11,1.11,0,0,1-.33.8,1.09,1.09,0,0,1-.8.33,1.06,1.06,0,0,1-.73-.27l-7.23-6.2-1.08.92v4.42a1.11,1.11,0,0,1-.33.8,1.09,1.09,0,0,1-.8.33,1.07,1.07,0,0,1-.79-.33,1.12,1.12,0,0,1-.34-.8Z" transform="translate(80 328)"/>
				</a>
				</g>
				</a>
<!--ФИОЛЕТОВАЯ -->
				<path fill="#fff" d="M160.46 425.62s-17.64-1.21-20.14 2.42 3.2 6.37 3.2 6.37-.67-8.1 16.94-8.79zM159.79 397.44s-12.84-2.08-15.82 3.09 3.5 7.1 6.12 4 2.07-7.76 9.7-7.09z" opacity=".4"/>
				<path fill="#fff" d="M150.1 409.68h7.45v14.07h-7.45zM156.84 524.01h-16.49l7.69-86.47h8.8v86.47z" opacity=".3"/>
				<a href="javascript:redirectToAsync('blog/Греция/index.html', 3000)">
				<g fill="#fff" xmlns:xlink="/contacts/" onclick="secondBoardClicked()" id="second-board">
						<path stroke="#fff" stroke-width="2" d="M3.94 160.2l34.54-35.1a8.24 8.24 0 0 1 5.86-2.47l226.14-1.2a8.32 8.32 0 0 1 8.3 8.25l.37 69.8a8.3 8.3 0 0 1-8.25 8.34l-226.1 1.2a8.24 8.24 0 0 1-5.84-2.4l-35-34.71a8.31 8.31 0 0 1-.02-11.72z"/>
						<path fill="#4b5d66" d="M7.63 163.84l34.59-35.11a3.12 3.12 0 0 1 2.2-.93l226.06-1.18a3.11 3.11 0 0 1 3.1 3.09l.38 69.79a3.11 3.11 0 0 1-3.09 3.13l-226.07 1.2a3.1 3.1 0 0 1-2.19-.9l-35-34.7a3.11 3.11 0 0 1 .02-4.4z"/>
						<path fill="#ca69c4" d="M44.43 130.94l226.07-1.22.38 69.79-226.08 1.21-34.95-34.71 34.58-35.07z"/>
						<path fill="#ca69c4" d="M270.5 129.72l-2.45 2.31-.55 63.97-224.63 2.81 1.93 1.91 226.06-.75-.36-70.25z"/>
						<g opacity=".2">
							<path d="M187.58 132.94l29.5-.16c4.84 2.81 28.9 17.27 34.56 30.5 6.3 14.72 14.69 32 14.69 32l-19.46.1s-12.63-17.9-23.19-24.52c-9.81-6.08-32.82-33.92-36.1-37.92zM169.98 133.03l50.37 62.53-55.85.31-29.25-62.65 34.73-.19zM157.77 195.91l-29.95.15-9.73-62.75 11.62-.06 28.06 62.66zM59.88 133.62l25.41-.13 2.75 62.79-39.41.21 11.25-62.87z"/>
						</g>
						<path d="M270.5 129.72l-2.45 2.31-222.57.94-35.63 33.04 34.58-35.07 226.07-1.22z" opacity=".2"/>
					</a>
				</g>
				<g xmlns:xlink="/contacts/" fill="#fff" onclick="secondBoardClicked()" id="closest-school">
					<defs>
						<style>.cls-1{fill:#fff;}</style>
					</defs>
					<path class="cls-1" d="M8.35,29.07V10a1,1,0,0,1,.32-.76A1,1,0,0,1,9.42,9H21.15a1.07,1.07,0,0,1,0,2.13H10.49V29.26h0a1,1,0,0,1-.3.74,1.07,1.07,0,0,1-.76.32A1,1,0,0,1,8.65,30a1,1,0,0,1-.31-.75A1.09,1.09,0,0,1,8.35,29.07Z" transform="translate(100 142)"/>
					<path class="cls-1" d="M16.58,35.65a1,1,0,0,1-.31.76,1.07,1.07,0,0,1-1.51,0,1,1,0,0,1-.31-.76V22.84a7.48,7.48,0,0,1,7.47-7.47,7.48,7.48,0,0,1,7.47,7.47,7.48,7.48,0,0,1-7.47,7.48,7.2,7.2,0,0,1-5.28-2.19l-.06-.06Zm0-12.81a5.34,5.34,0,0,0,5.34,5.34,5.33,5.33,0,0,0,5.34-5.34,5.33,5.33,0,0,0-5.34-5.33,5.34,5.34,0,0,0-5.34,5.33Z" transform="translate(100 142)"/>
					<path class="cls-1" d="M43.11,26.69a1.08,1.08,0,0,1,1.07,1.06,1.07,1.07,0,0,1-.31.76,7.47,7.47,0,0,1-12.34-5.67,7.47,7.47,0,0,1,12.75-5.28,7.2,7.2,0,0,1,2.19,5.28,1.06,1.06,0,0,1-1.07,1.07H33.76A5.34,5.34,0,0,0,39,28.18a5,5,0,0,0,3.44-1.27A1.08,1.08,0,0,1,43.11,26.69Zm1.13-4.91a5,5,0,0,0-1.47-2.71,5.35,5.35,0,0,0-7.55,0,5.07,5.07,0,0,0-1.46,2.71Z" transform="translate(100 142)"/>
					<path class="cls-1" d="M62.48,28.18h1.07a1.06,1.06,0,0,1,.75.31,1,1,0,0,1,.31.76,10.35,10.35,0,0,1-1,4.61,1,1,0,0,1-.25.4,1.07,1.07,0,0,1-1.83-.74,1.07,1.07,0,0,1,.12-.51,8.45,8.45,0,0,0,.79-2.69h-1a1.08,1.08,0,0,1-1.06-1.07v-.87a6.95,6.95,0,0,1-11.75-5V16.44a1.07,1.07,0,0,1,1.83-.75,1.06,1.06,0,0,1,.31.75v6.94a4.81,4.81,0,0,0,9.61,0V16.44a1.08,1.08,0,0,1,1.06-1.07,1,1,0,0,1,.76.32,1,1,0,0,1,.31.75Z" transform="translate(100 142)"/>
					<path class="cls-1" d="M80.62,29.25a1.08,1.08,0,0,1-1.06,1.07A1,1,0,0,1,78.8,30a1,1,0,0,1-.31-.75v-.87a6.94,6.94,0,0,1-11.74-5V16.44a1,1,0,0,1,.31-.75,1.05,1.05,0,0,1,1.51,0,1,1,0,0,1,.31.75v6.94a4.81,4.81,0,0,0,9.61,0V16.44a1,1,0,0,1,.31-.75,1.05,1.05,0,0,1,1.51,0,1,1,0,0,1,.31.75Z" transform="translate(100 142)"/>
					<path class="cls-1" d="M92.9,15.37A1.09,1.09,0,0,1,94,16.44V29.25a1.09,1.09,0,0,1-1.07,1.07,1,1,0,0,1-.75-.32,1,1,0,0,1-.32-.75V23.91h-3.2a2.05,2.05,0,0,0-1.51.63,2.09,2.09,0,0,0-.4.55l-2.44,4.56a1,1,0,0,1-.23.35,1.05,1.05,0,0,1-1.51,0,1,1,0,0,1-.31-.75,1,1,0,0,1,.17-.59l2.43-4.59a3.8,3.8,0,0,1,.52-.76,4.44,4.44,0,0,1-2.06-3.67,4.27,4.27,0,0,1,4.27-4.27Zm-1.07,6.41V17.51H87.56a2.14,2.14,0,0,0-1.51,3.64,2.07,2.07,0,0,0,1.51.63Z" transform="translate(100 142)"/>
				</g>
<!-- ЗЕЛЕНАЯ -->
				<a href="javascript:redirectToAsync('blog/Париж/index.html', 3000)">
					<g fill="#fff" xmlns:xlink="/moscow/" onclick="thirdBoardClicked()" id="third-board">
						<path stroke="#fff" stroke-width="2" d="M3.4 253.92l34.55-35.08a8.24 8.24 0 0 1 5.86-2.47l226.14-1.2a8.32 8.32 0 0 1 8.3 8.24l.37 69.8a8.3 8.3 0 0 1-8.25 8.34l-226.1 1.21a8.24 8.24 0 0 1-5.84-2.4l-35-34.72a8.31 8.31 0 0 1-.02-11.72z"/>
						<path fill="#4b5d66" d="M7.1 257.56l34.58-35.08a3.12 3.12 0 0 1 2.2-.93l226.07-1.2a3.11 3.11 0 0 1 3.1 3.1l.38 69.8a3.11 3.11 0 0 1-3.1 3.12l-226.06 1.21a3.11 3.11 0 0 1-2.2-.9l-35-34.7a3.11 3.11 0 0 1 .02-4.4z"/>
						<path fill="#9ed14f" d="M43.9 224.66l226.07-1.21.38 69.8-226.08 1.2-34.95-34.71 34.58-35.08z"/>
						<path fill="#9ed14f" d="M269.97 223.45l-2.45 2.3-.55 63.97-224.63 2.82 1.93 1.91 226.06-.75-.36-70.25z"/>
						<g opacity=".2">
							<path d="M187.05 226.67l29.5-.16c4.84 2.81 28.9 17.28 34.56 30.5 6.3 14.72 14.69 32 14.69 32l-19.46.1s-12.63-17.9-23.2-24.52c-9.8-6.08-32.81-33.9-36.1-37.92zM169.45 226.76l50.37 62.54-55.85.29-29.25-62.64 34.73-.19zM157.24 289.63l-29.95.16-9.73-62.75 11.62-.06 28.06 62.65zM59.35 227.35l25.41-.14L87.51 290l-39.41.21 11.25-62.86z"/>
						</g>
						<path d="M269.97 223.45l-2.45 2.3-222.57.95-35.63 33.04 34.58-35.08 226.07-1.21z" opacity=".2"/>
					</g>
					<g xmlns:xlink="/moscow/" fill="#4b5d66" onclick="thirdBoardClicked()" id="check-schedule">
						<defs>
							<style>.cls-1{fill:#fff;}</style>
						</defs>
						<path class="cls-1" d="M21.43,10.72a1,1,0,0,1,1,1V30.39a1,1,0,0,1-1,1,1,1,0,0,1-.73-.3,1,1,0,0,1-.31-.73V12.79H10v17.6a1,1,0,0,1-.3.73,1.05,1.05,0,0,1-1.47,0,1,1,0,0,1-.3-.73V11.75a1,1,0,0,1,.3-.73,1,1,0,0,1,.73-.3Z" transform="translate(100 238)"/>
						<path class="cls-1" d="M37,29.25a7.07,7.07,0,0,1-5.18,2.17,7.26,7.26,0,0,1-7.25-7.24,7.26,7.26,0,0,1,7.25-7.25A7.24,7.24,0,0,1,39,24.18v6.21a1,1,0,0,1-.3.73,1.05,1.05,0,0,1-1.47,0,1,1,0,0,1-.3-.73ZM31.78,19a5.16,5.16,0,0,0-5.17,5.18,5.18,5.18,0,0,0,8.84,3.66,5.2,5.2,0,0,0,0-7.33A5,5,0,0,0,31.78,19Z" transform="translate(100 238)"/>
						<path class="cls-1" d="M43.17,36.6a1,1,0,0,1-.3.73,1,1,0,0,1-1.47,0,1,1,0,0,1-.3-.73V24.18a7.25,7.25,0,0,1,12.37-5.13,7.23,7.23,0,0,1,0,10.25,7.25,7.25,0,0,1-10.25,0s0,0,0-.05Zm0-12.42A5.18,5.18,0,1,0,52,20.51a5.18,5.18,0,0,0-8.84,3.67Z" transform="translate(100 238)"/>
						<path class="cls-1" d="M71.13,30.39a1,1,0,0,1-.31.73,1,1,0,0,1-.73.3,1,1,0,0,1-1-1v-.84a6.73,6.73,0,0,1-11.39-4.86V18a1,1,0,0,1,.3-.73,1,1,0,0,1,.73-.3,1,1,0,0,1,1,1v6.73a4.66,4.66,0,0,0,9.32,0V18a1,1,0,0,1,.3-.73,1,1,0,0,1,.73-.3,1,1,0,0,1,1,1Z" transform="translate(100 238)"/>
						<path class="cls-1" d="M82.52,23.62V18a1,1,0,0,1,.3-.73,1,1,0,0,1,.73-.3,1,1,0,0,1,1,1v5.66l7.59-6.43a1,1,0,0,1,1.42,0,1,1,0,0,1,.31.73,1,1,0,0,1-.3.73l-6.44,5.45,6.44,5.51a1,1,0,0,1,.3.74,1,1,0,0,1-1,1,1,1,0,0,1-.67-.24L85.57,25.5l-1,.84v4.05a1,1,0,0,1-.31.73,1,1,0,0,1-.73.3,1,1,0,0,1-1-1V26.34l-1-.84-6.62,5.68a1,1,0,0,1-.68.24,1,1,0,0,1-1-1,1,1,0,0,1,.3-.74l6.43-5.51L73.5,18.69a1,1,0,0,1-.3-.73,1,1,0,0,1,.3-.73,1,1,0,0,1,1.42,0Z" transform="translate(100 238)"/>
					</g>
				</a>
			</svg>
		</div>
<!-- Правильное отображение -->
</div>
