<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О нас';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <body class="page-template page-template-template-page-history page-template-template-page-history-php page page-id-3595">

	<link rel="stylesheet" href="../Gothetour/css/style68b3.css?ver=1" type="text/css">
	
	<div id="intro">
		<div id="container-middle-top-edge"></div>
		<div id="container-middle-bottom-edge"></div>
	</div>
	<div id="content" class="about">
	<div class="wrap">
	<div class="container">
	<div class="row">
	<div class="col-xs-12">
	<div class="page page-content">
	<div class="about-text" >
	<h1 style="font-family: 'Comfortaa', cursive;margin-left: 550px; color: orange;" class="title" style="width: 500px;">О компании</h1><br>
	<h4 style="width: 1200px; margin-left:50px; text-align: center;">Туристическая компания «GOTHETOUR» создана в 2018 году. Целью создания компании было оказание туристических услуг населению. Все это время компания «GOTHETOUR» успешно работает на рынке туристических услуг и предлагает населению как отдых, так и экскурсионные туры. Среди туров предлагаемых нашей компанией каждый желающий может выбрать именно свой тур , а наши специалисты помогут и окажут необходимую консультационную помощь в подборе тура. Для этой цели мы разрабатываем и предлагаем на наш рынок такие направления для отдыха как Турция, Египет, Кипр, Греция, Испания, Болгария, Черногория, Хорватия, ОАЭ, Таиланд, Куба, Доминикана, Мальдивы, Индонезия, Гоа, Малайзия,  туры в  Крым, Украину, Литву, Латвию, Польшу, а так же обширную экскурсионную программу по странам Восточной и Западной Европы. 
	Кроме того, мы активно принимаем участие в разработке и продвижении на международном рынке туристических услуг прием и обслуживание в Беларуси. Для этой цели наши специалисты разрабатывают экскурсионные программы, которые мы предлагаем как для групп, так и для индивидуальных туристов.
	«GOTHETOUR» - это компания, которая рада каждому туристу и не оставит его без внимания, которая имеет немало постоянных клиентов и партнеров и будет рада каждому туристу, который решит обратиться к нашим специалистам.
</h4><br> 

<h2 style="font-family: 'Comfortaa', cursive; text-align:center;margin-left:50px;">Видео о турфирме "GoTheTour"</h2>
<br>

<div style=" display: -webkit-inline-flex;
 display: inline-flex; ">
<p style="margin-right:50px; margin-left:70px;"><iframe width="560" height="315"  src="https://www.youtube.com/embed/ZUHrr7EYWPk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
	<p style="margin-right:50px;"><iframe width="560"  height="315" src="https://www.youtube.com/embed/f_AFJxKUeD8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
</div>
</body>


</div>
