<?php
use yii\helpers\Url;
?>
<div class="col-md-4" data-sticky_column>
    <div class="primary-sidebar">

        
        <aside class="widget pos-padding">
            <h3 class="widget-title text-uppercase text-center">Последние посты</h3>
            <?php foreach($recent as $tour):?>
                <div class="thumb-latest-posts">
                    <div class="media">
                        <div class="media-left">
                            <a href="<?= Url::toRoute(['site/singletour','id'=>$tour->id]);?>" class="popular-img"><img src="<?= $tour->getImage();?>" alt="">
                                <div class="p-overlay"></div>
                            </a>
                        </div>
                        <div class="p-content">
                            <a href="<?= Url::toRoute(['site/singletour','id'=>$tour->id]);?>" class="text-uppercase"><?= $tour->title?></a>
                            <span class="p-date"><?= $tour->getDate();?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </aside>
        <aside class="widget border pos-padding">
            <h3 class="widget-title text-uppercase text-center">Категории</h3>
            <ul>
                <?php foreach($categories as $category):?>
                    <li>
                        <a href="<?= Url::toRoute(['site/category','id'=>$category->id]);?>"><?= $category->title?></a>
                        <span class="post-count pull-right"> (<?= $category->getTourCount();?>)</span>
                    </li>
                <?php endforeach;?>

            </ul>
        </aside>
    </div>
</div>