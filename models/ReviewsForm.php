<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ReviewsForm extends Model
{
    public $review;
    
    public function rules()
    {
        return [
            [['review'], 'required'],
            [['review'], 'string', 'length' => [3,250]]
        ];
    }

    public function saveReviews()
    {
        $review = new Reviews;
        $review->title = $this->review;
        $review->text = $this->review;
        $review->user_id = Yii::$app->user->id;
        $review->status = 0;
        $review->date = date('Y-m-d');
        return $review->save();

    }
}