<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Reservation;
use yii\data\Pagination;
use app\models\Gallery;
/**
 * This is the model class for table "tour".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $content
 * @property string|null $date
 * @property int|null $gallery_id
 * @property string|null $image
 * @property int|null $viewed
 * @property int|null $user_id
 * @property int|null $status
 * @property int|null $category_id
 * @property int|null $rating_id
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'],'required'],
            [['title','description', 'content'], 'string'],
            [['date'], 'date', 'format'=>'php:Y-m-d'],
            [['date'],'default','value'=>date('Y-m-d')],
            [['gallery_id', 'viewed', 'user_id', 'status', 'category_id', 'rating_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
                        
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'date' => 'Date',
            'gallery_id' => 'Gallery ID',
            'image' => 'Image',
            'viewed' => 'Viewed',
            'user_id' => 'User ID',
            'status' => 'Status',
            'category_id' => 'Category ID',
            'rating_id' => 'Rating ID',
        ];
    }
    public function saveImage($filename){
        $this->image = $filename;
        return $this->save(false);
    }
    
    public function getImage(){
    
    
     if($this->image){
        return ' http://localhost/Gothetour/basic/web/uploads/'. $this->image;
     }
     return ' http://localhost/Gothetour/basic/web/uploads/no-image.png';
    }

    public function deleteImage(){
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    public function beforeDelete(){
        $this->deleteImage();
        return parent::beforeDelete();
    }
    
    /*category*/ 
    public function getCategory()
    {                                       //поле в табл категории и поле в табл туры
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }


    public function saveCategory($category_id)
    {
        $category = Category::findOne($category_id);
        if($category != null)
        {
            $this->link('category', $category);
            return true;            
        }
    }
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('tour_tag', ['tour_id' => 'id']);
    }
    public function getSelectedTags()
    {
         $selectedIds = $this->getTags()->select('id')->asArray()->all();
        return ArrayHelper::getColumn($selectedIds, 'id');
    }
    public function saveTags($tags)
    {
        if (is_array($tags))
        {
            $this->clearCurrentTags();

            foreach($tags as $tag_id)
            {
                $tag = Tag::findOne($tag_id);
                $this->link('tags', $tag);
            }
        }
    }
    public function clearCurrentTags()
    {
        TourTag::deleteAll(['tour_id'=>$this->id]);
    }

    public function getDate(){
        return Yii::$app->formatter->asDate($this->date);
    }
    public function getTourCount(){
        return $this->getTour()->count();
    }
    public static function getAll($pageSize = 5){

        // $query = Tour::find();
        // $count = $query->count();
        // $pagination = new Pagination(['totalCount' => $count,'pageSize'=>$pageSize]);
        // $tours = $query->offset($pagination->offset)
        //     ->limit($pagination->limit)
        //     ->all();
        // $data['tours'] = $tours;
        // $data['pagination']= $pagination;

        // return $data;
    }
    public static function getPopular(){
        return Tour::find()->orderBy('viewed desc')->limit(3)->all();
    }
    public static function getRecent(){
        return Tour::find()->orderBy('date asc')->limit(4)->all();
    }

    public function saveTour(){
        $this->user_id=Yii::$app->user->id;
        return $this->save();
    }
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['tour_id'=>'id']);
    }

    public function getTourComments()
    {
        return $this->getComments()->where(['status'=>1])->all();
    }

    public function getGallery() {
        $gallery = Gallery::find()->where(['tour_id'=>$this->id])->one();
        if($gallery != null){
            return json_decode($gallery->images, true);
        }
    }

    public function getAvailablePlace() {
        $sum = 0;
        $reservations = Reservation::find()->where(['tour_id'=>$this->id])->all();
        if (is_array($reservations) && !empty($reservations)) {
            foreach ($reservations as $reservation) {
                $sum += $reservation->count;
            }
            return $this->count_place-$sum;
        } else {
            return $this->count_place;  
        }      
    }
}
