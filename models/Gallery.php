<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property int $id
 * @property int|null $tour_id
 * @property string|null $images
 * @property string|null $title
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id'], 'integer'],
            [['images', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Tour ID',
            'images' => 'Images',
            'title' => 'Title',
        ];
    }

    public function getByTour($id) {
        $query = Gallery::find()->where(['tour_id'=>$id])->one();
    }

    public function saveGallery($id, $files){
        if (($model = Gallery::find()->where('tour_id=:id',[':id'=>$id])->one()) !== null) {
            $this->removeAllWithTour($id);
        }
        $this->tour_id = $id;
        $this->images = json_encode($files);
        return $this->save();
    }

    public function removeAllWithTour($id) {
        Gallery::deleteAll(['tour_id'=>$id]);
    }
}
