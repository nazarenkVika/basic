<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\ImageUpload;
use Yii;    
class GalleryUpload extends Model{

    public $images;
    public $image;

    
    public function rules(){
            return[
                [['image'], 'required'],
                [['image'], 'file', 'extensions'=>'jpg,png'],
                [['images'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 4]
            ];
    }

    public function uploadFiles($files){
        $this->images = $files;
        $loader = new ImageUpload();
        foreach($this->images as $image) {
            $filenames[] = $this->uploadFile($image);
        }
        return $filenames;
    }

    public  function  uploadFile(UploadedFile $file){
        $this->image = $file;
        if($this->validate()){
            
            $this->deleteCurrentImage($file);

            return $this->saveImage($this->image);
            
        }
    }
    public function getFolder(){
        return Yii::getAlias('@app/web') . '/uploads/';
    }

    public function generateFilename(){
        return strtolower(md5(uniqid($this->image->baseName))) .'.'. $this->image->extension;
    }
/*для того, чтобы перезаписать картинку имеющуюся для статьи*/
    public function deleteCurrentImage($currentImage){
        if ($this->fileExists($currentImage)){
            unlink($this->getFolder(). $currentImage);
        }
    }

    public function fileExists($currentImage){
        if(!empty($currentImage) && $currentImage != null){
            return file_exists($this->getFolder(). $currentImage);
        }
    }

    public function saveImage($currentImage){
        $filename = $this->generateFilename();
        $this->image->saveAs($this->getFolder(). $filename);
        return $filename;
    }
}