<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $text
 * @property int|null $user_id
 * @property int|null $status
 * @property string|null $date
 */
class Reviews extends \yii\db\ActiveRecord
{
    const STATUS_ALLOW = 1;
    const STATUS_DISALLOW = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['date'], 'date', 'format'=>'php:Y-m-d'],
            [['date'],'default', 'value'=>date('Y-m-d')],
            [['text','title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title'=>'Title',
            'text' => 'Text',
            'user_id' => 'User ID',
            'status' => 'Status',
            'date' => 'Date',
        ];
    }
    public static function getAll()
    {
        
        return Reviews::find()->all();

    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->date);
    }

    public function getReview()
    {
        return $this->getComments()->where(['status'=>1])->all();

    }
    public function isAllowed()
    {
        return $this->status;
    }
    public function allow()
    {
        $this->status = self::STATUS_ALLOW;
        return $this->save(false);
    }

    public function disallow()
    {
        $this->status = self::STATUS_DISALLOW;
        return $this->save(false);
    }

  
}
