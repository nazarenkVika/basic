<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string|null $title
 */
class SearchForm extends \yii\db\ActiveRecord
{
    public $query;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['query', 'string'],
        ];
    }

    // public static function getAll($pageSize = 5){

        // $query = Tour::find()->where(['like', 'title', $query]);
        // $count = $query->count();
        // $pagination = new Pagination(['totalCount' => $count,'pageSize'=>$pageSize]);
        // $tours = $query->offset($pagination->offset)
        //     ->limit($pagination->limit)
        //     ->all();
        // $data['tours'] = $tours;
        // $data['pagination']= $pagination;
        
        // return $data;
        // }

}
