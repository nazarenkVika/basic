<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reservation".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $tour_id
 */
class Reservation extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 0;
    const STATUS_DECLINED = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_FINISHED = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reservation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'tour_id', 'count', 'status'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'tour_id' => 'Tour ID',
            'tour_id' => 'Tour ID',
            'tour_id' => 'Tour ID'
        ];
    }

    public static function getActiveReservation($id) {
        return Reservation::find()->where(['user_id'=>Yii::$app->user->id, 'tour_id'=>$id])->andWhere(['in', 'status', [self::STATUS_PENDING, self::STATUS_ACCEPTED, self::STATUS_FINISHED]])->one();
    }
}
