<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tour_tag".
 *
 * @property int $id
 * @property int|null $tour_id
 * @property int|null $tag_id
 */
class TourTag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour_tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id', 'tag_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Tour ID',
            'tag_id' => 'Tag ID',
        ];
    }
}
