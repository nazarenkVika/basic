<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
/**
 * This is the model class for table "tour".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $content
 * @property string|null $date
 * @property int|null $gallery_id
 * @property string|null $image
 * @property int|null $viewed
 * @property int|null $user_id
 * @property int|null $status
 * @property int|null $category_id
 * @property int|null $rating_id
 */
class Search extends \yii\db\ActiveRecord
{
public static function getAll($pageSize = 5){

$query = Tour::find();
$count = $query->count();
$pagination = new Pagination(['totalCount' => $count,'pageSize'=>$pageSize]);
$tours = $query->offset($pagination->offset)
    ->limit($pagination->limit)
    ->all();
$data['tours'] = $tours;
$data['pagination']= $pagination;

return $data;
}
}