<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ReservationForm extends Model
{
    public $count;
    
    public function rules()
    {
        return [
            [['tour_id', 'count'], 'required'],
            [['tour_id', 'count'], 'integer']
        ];
    }

    public function saveReservation($tour_id)
    {
        $reservation = new Reservation;
        $reservation->tour_id = $tour_id;
        $reservation->user_id = Yii::$app->user->id;
        $reservation->count = $this->count;
        $reservation->status = 0;
        return $reservation->save();

    }
}