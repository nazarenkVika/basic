<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $email
 * @property string|null $login
 * @property string|null $password
 * @property string|null $phone
 * @property int|null $isAdmin
 * @property string|null $photo
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isAdmin'], 'integer'],
            [['name', 'surname', 'email', 'login', 'password', 'phone', 'photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'email' => 'Email',
            'login' => 'Login',
            'password' => 'Password',
            'phone' => 'Phone',
            'isAdmin' => 'Is Admin',
            'photo' => 'Photo',
        ];
    }
    public static function findIdentity($id)
    {
        return User::findOne($id);
    }
    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    public static function findByEmail($email){
        return User::find()->where(['email'=>$email])->one();
    }

    public function validatePassword($password){
        if($this->password == $password)
        {
            return true;
        }
        else{
            return false;
        }

    }

    public function create(){

        return $this->save(false);

    }

    public function getImage(){
    
    
        if($this->photo){
           return ' http://localhost/Gothetour/basic/web/uploads/'. $this->image;
        }
        return ' http://localhost/Gothetour/basic/web/uploads/no-image.png';
   }
   
}
